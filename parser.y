%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "util.h"
#include "expr.h"

#define YYDEBUG 1

  extern int yylex();
  extern int yyerror();

  extern List type_list;
  extern List func_list_list;
%}

%token L_TYPE
%token L_OF
%token L_LET
%token L_REC
%token L_FUNCTION
%token L_AND
%token L_ARROW
%token <string> L_STRING
%token <string> L_IDENT

%type <string>           const_name
%type <type>             type
%type <list>             type_exprs
%type <string>           type_name
%type <list>             consts
%type <list>             const_list
%type <type_const>       type_const
%type <list>             func
%type <list>             let_rec_body
%type <func_body>        func_body
%type <list>             const_matches
%type <list>             const_match_list
%type <const_match>      const_match
%type <lhs>              const_match_left
%type <rhs_element>      rhs_element
%type <list>             rhs_elements
%type <string>           func_name
%type <string>           variable
%type <list>             variables

%union {
  char*      string;
  List       list;
  Type       type;
  TypeConst  type_const;
  FuncBody   func_body;
  ConstMatch const_match;
  Lhs        lhs;
  RhsElement rhs_element;
}
%%

progs             : expr progs
                  |
                  ;
expr              : type {
                    list_cons(&$1, &type_list);
                  }
                  | func {
                    list_cons(&$1, &func_list_list);
                  }
                  | embed_func {
                  }
                  ;
type              : L_TYPE type_name '=' consts {
                    $$.type_name = $2;
                    $$.consts = $4;
                  }
                  ;
consts            : const_list {
                    $$ = $1;
                  }
                  | '|' const_list {
                    $$ = $2;
                  }
                  ;
const_list        : type_const '|' const_list {
                    $$ = $3;
                    list_cons(&$1, &$$);
                  }
                  | type_const {
                    list_init(&$$, sizeof(TypeConst), type_const_free);
                    list_cons(&$1, &$$);
                  }
                  ;
type_name         : L_IDENT {
                    $$ = string_copy($1);
                  }
                  ;
type_const        : const_name L_OF type_exprs {
                    $$.const_name = $1;
                    $$.type_exprs = $3;
                  }
                  | const_name {
                    $$.const_name = $1;
                    list_init(&$$.type_exprs, sizeof(char*), string_free);
                  }
                  ;
type_exprs        : type_name '*' type_exprs {
                    $$ = $3;
                    list_cons(&$1, &$$);
                  }
                  | type_name {
                    list_init(&$$, sizeof(char*), string_free);
                    list_cons(&$1, &$$);
                  }
                  ;
const_name        : L_IDENT {
                    $$ = string_copy($1);
                  }
                  ;
func              : L_LET L_REC func_body L_AND let_rec_body {
                    $$ = $5;
                    list_cons(&$3, &$$);
                  }
                  | L_LET L_REC func_body {
                    list_init(&$$, sizeof(FuncBody), func_body_free);
                    list_cons(&$3, &$$);
                  }
                  | L_LET func_body {
                    list_init(&$$, sizeof(FuncBody), func_body_free);
                    list_cons(&$2, &$$);
                  }
                  ;
let_rec_body      : func_body L_AND let_rec_body {
                    $$ = $3;
                    list_cons(&$1, &$$);
                  }
                  | func_body {
                    list_init(&$$, sizeof(FuncBody), func_body_free);
                    list_cons(&$1, &$$);
                  }
                  ;
func_body         : func_name '=' L_FUNCTION const_matches {
                    $$.func_name = $1;
                    $$.const_matches = $4;
                  }
                  ;
func_name         : L_IDENT {
                    $$ = string_copy($1);
                  }
                  ;
const_matches     : const_match_list {
                    list_init(&$$, sizeof(ConstMatch), const_match_free);
                    list_cons(&$$, &$1);
                  }
                  | '|' const_match_list {
                    $$ = $2;
                  }
                  ;
const_match_list  : const_match '|' const_match_list {
                    $$ = $3;
                    list_cons(&$1, &$$);
                  }
                  | const_match {
                    list_init(&$$, sizeof(ConstMatch), const_match_free);
                    list_cons(&$1, &$$);
                  }
                  ;
const_match       : const_match_left L_ARROW rhs_elements {
                    $$.lhs = $1;
                    $$.rhs = $3;
                  }
                  ;
const_match_left  : const_name {
                    $$.const_name = $1;
                  }
                  | const_name '(' variables ')' {
                    $$.const_name = $1;
                    $$.variables  = $3;
                  }
                  ;
rhs_elements      : rhs_element {
                    if(!(($1.tag == RHS_OUTPUT) && (strcmp($1.output, "\"\"") == 0))) {
                      list_init(&$$, sizeof(RhsElement), rhs_element_free);
//                      if($1.tag == RHS_OUTPUT && strcmp($1.output, "\"\"") != 0) {
                        list_cons(&$1, &$$);
//                      }
                    }
                  }
                  | rhs_element '^' rhs_elements {
                    $$ = $3;
                    if(!(($1.tag == RHS_OUTPUT) && (strcmp($1.output, "\"\"") == 0))) {
                      list_cons(&$1, &$$);
                    }
                  }
                  ;
rhs_element       : L_STRING {
                    $$.tag    = RHS_OUTPUT;
                    $$.output = string_copy($1);
                  }
                  | func_name '(' variable ')' {
                    $$.tag                = RHS_CALL;
                    $$.rhs_call.func_name = $1;
                    $$.rhs_call.variable  = $3;
                  }
                  ;
variable          : L_IDENT {
                    $$ = string_copy($1);
                  }
                  ;
variables         : variable ',' variables {
                    $$ = $3;
                    list_cons(&$1, &$$);
                  }
                  | variable {
                    list_init(&$$, sizeof(char*), string_free);
                    list_cons(&$1, &$$);
                  }
                  ;
embed_func        : func_name '(' func_name ',' func_name ')' {
                  }
                  ;
%%
