#ifndef _POLY_H_
#define _POLY_H_

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>

#include "list.h"

typedef struct {
  int*  mi;    // multi-index noation
  int   n;     // n-dimension
  mpz_t coef;  // coefficient
} Term;

void term_init(Term* term, int n);
void term_set(Term* dst, Term* src);
void term_free(void* data);
void* list_copy_term(void* data);

void term_set_mpz(Term* term, mpz_t coef);
void term_set_str(Term* term, char *coef);
void term_set_ui(Term *term, unsigned long int coef);

void term_neg(Term* term);
void term_mul_t(Term* l, Term* r);
void term_mul_mpz(Term* l, mpz_t z);

void term_print(Term* term);

// lex 順序
int term_lex_cmp(void *data1, void* data2);

// rt can devide lt.
bool term_can_devide(Term* dividend, Term* divisor);

/* 項 p と q が与えられたとき，
 * p = q * r
 * を求める．
 * r の係数は LC(q)/lcm(p,q) とする．
 * また，割り切れる前提で計算を行う．
 * ここでは，整数の範囲で計算を行いたいため，r の係数が分数となる場合には，逆数を返す．
 * LC(p) = LC(q) * LC(r)
 * となるかを計算することで，逆数を返してきたか判定する
 */
void term_devide(Term* quotient, Term* dividend, Term* divisor);

int term_degree(Term *term);

typedef struct {
  int  n;
  List terms;
} Poly;

void poly_init(Poly* poly, int n);
void poly_free(void* data);
void poly_set(Poly* dst, Poly* src);

void poly_one(Poly* poly);

void poly_neg(Poly *poly);
void poly_add_z(Poly* poly, mpz_t z);
void poly_add_ui(Poly* poly, unsigned long int z);
void poly_add_t(Poly* poly, Term* r);
void poly_add_p(Poly* l, Poly* r);

void poly_sub_ui(Poly* poly, unsigned long int z);
void poly_sub_t(Poly* poly, Term* r);
void poly_sub_p(Poly* l, Poly* r);

void poly_mul_mpz(Poly* poly, mpz_t z);
void poly_mul_t(Poly* poly, Term* t);
void poly_mul_p(Poly* l, Poly* r);
void poly_mul_ui(Poly* poly, unsigned long int z);

void poly_print(Poly* poly);
void* list_copy_poly(void* data);

void dummy_variables(char ***vars_p, int size);
void dummy_variables_free(char ***vars_p, int size);

void poly_lt(Term* lt, Poly* poly);
bool poly_is_zero(Poly* poly);

typedef struct {
  mpz_t normalizer;
  Poly  quotient;
  Poly  remainder;
} Quorem;

void quorem_init(Quorem* quorem, int n);

void quorem_free(void* quorem);

// dividend = (quotient * divisor + remainder) / normalizer
bool poly_divide(Quorem* quorem,
                 Poly* dividend,
                 Poly* divisor);

bool poly_can_divide(Poly* dividend, Poly* divisor);

int poly_total_degree(Poly* poly);

int poly_cmp(void* data1, void* data2);

#endif
