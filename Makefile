LIBDIR := x64
# LIBDIR := macosx

CC       = gcc
CPP      = g++
CPPFLAGS = -Wall -O3
CFLAGS   = -Wall -O3
LDLIBS   = -ll -ly -lgmp -lfgb -lfgbexp -lgb -lgbexp -lminpoly -lminpolyvgf -lm -fopenmp

LEX  = flex
YACC = bison

.PHONY: all

all: main.o test.o list.o util.o expr.o ydt.o poly.o ydt_to_poly.o ideal.o protocol_maple.o call_fgb.o algorithm.o groebner.o
	$(CPP) $(CPPFLAGS) $^ $(LDLIBS) -L$(LIBDIR)

main.o: main.c lex.yy.c parser.tab.c
	$(CC) $(CFLAGS) -c $<

lex.yy.c: lexer.l
	$(LEX) $<

parser.tab.c: parser.y
	$(YACC) $<

groebner.o: groebner.c groebner.h
	$(CC) -c $< -I ./protocol -I ./int -I ./

protocol_maple.o: ./int/protocol_maple.c ./int/protocol_maple.h
	$(CC) -c $< -I ./protocol -I ./int -I ./

call_fgb.o: call_fgb.c call_fgb.h
	$(CC) -c $< -I ./protocol -I ./int -I ./

.SUFFIXES: .c .o

.c .o .h:
	$(CC) -c $<

.PHONY: clean
clean:
	$(RM) -rf *.o *.out
