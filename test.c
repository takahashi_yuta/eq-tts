#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>

#include "list.h"
#include "util.h"
#include "poly.h"
#include "ydt.h"
#include "ydt_to_poly.h"
#include "ideal.h"
#include "groebner.h"
#include "algorithm.h"

#include "test.h"

void test_list() {
  printf(">>>>>>>>>> test_list >>>>>>>>>>\n");

  List list;
  list_init(&list, sizeof(char*), string_free);

  char* str1 = "1";
  char* str2 = "2";
  char* str3 = "3";
  char* str;

  str = strdup(str1);
  list_cons(&str, &list);

  str = strdup(str2);
  list_cons(&str, &list);

  str = strdup(str3);
  list_cons(&str, &list);

  list_foreach(&list, string_print_itr);

  List copy;
  list_init(&copy, sizeof(char*), string_free);

  list_copy(&copy, &list, list_copy_string);

  list_foreach(&copy, string_print_itr);

  list_free(&list);

  printf("<<<<<<<<<< test_list <<<<<<<<<<\n");
}

void test_term() {
  printf(">>>>>>>>>> test_term >>>>>>>>>>\n");
  Term term;
  term_init(&term, 3);

  // term_init, term_print
  term_print(&term);
  newline();

  mpz_t z;
  mpz_init(z);
  mpz_set_str(z, "14", 10);

  // term_set
  term_set_mpz(&term, z);

  term_print(&term);
  newline();

  term.mi[1] = 2;

  term_print(&term);
  newline();

  // term_copy
  Term copy;
  term_init(&copy, term.n);
  term_set(&copy, &term);

  term_print(&copy);
  newline();

  // term_set_str
  term_set_str(&term, "2");

  term_print(&term);
  newline();

  // term_set_ui
  term_set_ui(&term, 10);

  term_print(&term);
  newline();

  // term_neg
  Term neg;
  term_init(&neg, copy.n);
  term_set(&neg, &copy);
  term_neg(&neg);

  term_print(&neg);
  newline();

  neg.mi[2] = 1;

  term_print(&neg);
  newline();

  term_print(&copy);
  newline();

  term_neg(&copy);
  term_print(&copy);
  newline();

  Term mul;
  term_init(&mul, neg.n);
  term_set(&mul, &neg);
  term_mul_t(&mul, &neg);

  term_print(&mul);
  newline();

  term_mul_t(&mul, &neg);

  term_print(&mul);
  newline();

  term_free(&term);
  mpz_clear(z);
  term_free(&copy);
  term_free(&neg);
  term_free(&mul);
  printf("<<<<<<<<<< test_term <<<<<<<<<<\n");
}

void test_poly() {
  printf(">>>>>>>>>> test_poly >>>>>>>>>>\n");
  // poly_add_ui
  Poly poly;
  poly_init(&poly, 3);

  poly_add_ui(&poly, 4);

  poly_print(&poly);
  newline();

  // poly_add_t
  Term term;
  term_init(&term, 3);
  term.mi[0] = 1;
  term.mi[1] = 2;
  term.mi[2] = 3;

  poly_add_t(&poly, &term);

  poly_print(&poly);
  newline();

  term_set_ui(&term, 10);
  poly_add_t(&poly, &term);

  poly_print(&poly);
  newline();

  Term neg;
  term_init(&neg, term.n);
  term_set(&neg, &term);
  term_neg(&neg);

  poly_add_t(&poly, &neg);

  poly_print(&poly);
  newline();

  Poly copy;
  poly_init(&copy, poly.n);
  poly_set(&copy, &poly);

  poly_print(&copy);
  newline();

  poly_add_t(&copy, &neg);
  poly_add_t(&copy, &neg);

  poly_print(&copy);
  newline();

  Poly add;
  poly_init(&add, poly.n);
  poly_set(&add, &poly);
  poly_add_p(&add, &copy);

  poly_print(&add);
  newline();

  Poly neg_poly;
  poly_init(&neg_poly, add.n);
  poly_set(&neg_poly, &add);
  poly_neg(&neg_poly);

  poly_print(&neg_poly);
  newline();

  poly_mul_p(&neg_poly, &add);

  poly_print(&neg_poly);
  newline();

  poly_free(&poly);
  term_free(&term);
  term_free(&neg);
  poly_free(&copy);
  poly_free(&add);
  poly_free(&neg_poly);
  printf("<<<<<<<<<< test_poly <<<<<<<<<<\n");
}

void test_expr_to_ydt(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_expr_to_ydt >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);
  list_foreach(&ydts, ydt_print);
  list_free(&ydts);
  printf("<<<<<<<<<< test_expr_to_ydt <<<<<<<<<<\n");
}

void test_ydt_merge(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_ydt_merge >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);

  Ydt* ydt1 = (Ydt*) ydts.head->data;
  Ydt* ydt2 = (Ydt*) ydts.head->next->data;

  Ydt merge;
  ydt_init(&merge);
  ydt_merge(&merge, ydt1, ydt2);

  ydt_print(&merge);
  ydt_print(ydt1);
  ydt_print(ydt2);

  list_free(&ydts);
  ydt_free(&merge);
  printf("<<<<<<<<<< test_ydt_merge <<<<<<<<<<\n");
}

void test_ydt_to_poly(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_ydt_to_poly >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);

  YdtPoly ydt_poly;
  ydt_poly_init(&ydt_poly);

  ydt_print((Ydt*) ydts.head->data);

  ydt_to_ydt_poly(&ydt_poly, (Ydt*) ydts.head->data);

  ydt_poly_print(&ydt_poly);

  Ydt merge;
  ydt_init(&merge);
  Ydt* ydt1 = (Ydt*) ydts.head->data;
  Ydt* ydt2 = (Ydt*) ydts.head->next->data;
  ydt_merge(&merge, ydt1, ydt2);

  ydt_print(&merge);

  YdtPoly merge_poly;
  ydt_poly_init(&merge_poly);
  ydt_to_ydt_poly(&merge_poly, &merge);
  ydt_poly_print(&merge_poly);

  list_free(&ydts);
  ydt_poly_free(&ydt_poly);
  ydt_free(&merge);
  ydt_poly_free(&merge_poly);
  printf("<<<<<<<<<< test_ydt_to_poly <<<<<<<<<<\n");
}

void test_groebner() {
  printf(">>>>>>>>>> test_groebner >>>>>>>>>>\n");
  Poly f1;
  poly_init(&f1, 2);

  Term xy;
  term_init(&xy, 2);
  term_set_ui(&xy, 1);
  xy.mi[0] = 1;
  xy.mi[1] = 1;
  poly_add_t(&f1, &xy);
  term_free(&xy);

  Term one;
  term_init(&one, 2);
  term_set_ui(&one, 1);
  poly_add_t(&f1, &one);
  term_free(&one);

  Poly f2;
  poly_init(&f2, 2);

  Term y2;
  term_init(&y2, 2);
  term_set_ui(&y2, 1);
  xy.mi[1] = 2;
  poly_add_t(&f2, &y2);
  term_free(&y2);

  Term neg_one;
  term_init(&neg_one, 2);
  term_set_ui(&neg_one, 1);
  term_neg(&neg_one);
  poly_add_t(&f2, &neg_one);
  term_free(&neg_one);

  Ideal ideal;
  ideal_init(&ideal, 2);
  list_cons(&f1, &ideal.base);
  list_cons(&f2, &ideal.base);

  ideal_print(&ideal);
  newline();

  Ideal groebner;
  ideal_init(&groebner, 2);
  ideal_groebner(&groebner, &ideal, 0);

  ideal_print(&groebner);
  newline();

  ideal_free(&ideal);
  ideal_free(&groebner);
  printf("<<<<<<<<<< test_groebner <<<<<<<<<<\n");
}

void test_intersect() {
  printf(">>>>>>>>>> test_intersect >>>>>>>>>>\n");
  Ideal I;
  ideal_init(&I, 3);
  Term x2y;
  term_init(&x2y, 3);
  term_set_ui(&x2y, 1);
  x2y.mi[1] = 2;
  x2y.mi[2] = 1;
  Poly f1;
  poly_init(&f1, 3);
  poly_add_t(&f1, &x2y);
  list_cons(&f1, &I.base);

  ideal_print(&I);
  newline();

  Ideal J;
  ideal_init(&J, 3);
  Term xy2;
  term_init(&xy2, 3);
  term_set_ui(&xy2, 1);
  xy2.mi[1] = 1;
  xy2.mi[2] = 2;
  Poly f2;
  poly_init(&f2, 3);
  poly_add_t(&f2, &xy2);
  list_cons(&f2, &J.base);

  ideal_print(&J);
  newline();

  Ideal intersect;
  ideal_init(&intersect, 3);
  ideal_intersect(&intersect, &I, &J);

  ideal_print(&intersect);
  newline();

  ideal_free(&I);
  term_free(&x2y);
  ideal_free(&J);
  term_free(&xy2);
  ideal_free(&intersect);
  printf("<<<<<<<<<< test_intersect <<<<<<<<<<\n");
}

void test_sharp(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_sharp >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);

  YdtPoly ydt_poly;
  ydt_poly_init(&ydt_poly);

  ydt_print((Ydt*) ydts.head->data);

  ydt_to_ydt_poly(&ydt_poly, (Ydt*) ydts.head->data);

  ydt_poly_print(&ydt_poly);

  Ydt merge;
  ydt_init(&merge);
  Ydt* ydt1 = (Ydt*) ydts.head->data;
  Ydt* ydt2 = (Ydt*) ydts.head->next->data;
  ydt_merge(&merge, ydt1, ydt2);

  ydt_print(&merge);

  YdtPoly merge_poly;
  ydt_poly_init(&merge_poly);
  ydt_to_ydt_poly(&merge_poly, &merge);
  ydt_poly_print(&merge_poly);

  int dim = dimension(merge_poly.states_size, merge_poly.max_rank);

  Ideal ideal_sharp_f;
  ideal_init(&ideal_sharp_f, dim);
  YdtInput* input_f = (YdtInput*) merge_poly.inputs.head->data;
  List ideals_3;
  list_init(&ideals_3, sizeof(Ideal), ideal_free);
  Ideal I1, I2, I3;
  ideal_init(&I1, dim);
  ideal_init(&I2, dim);
  ideal_init(&I3, dim);
  ideal_one(&I1);
  ideal_one(&I2);
  ideal_one(&I3);
  list_cons(&I1, &ideals_3);
  list_cons(&I2, &ideals_3);
  list_cons(&I3, &ideals_3);

  sharp(&ideal_sharp_f, &merge_poly, input_f, &ideals_3);

  printf("[[f]]#: ");
  ideal_print(&ideal_sharp_f);
  newline();

  YdtInput* input_e = (YdtInput*) merge_poly.inputs.head->next->data;
  Ideal ideal_sharp_e;
  ideal_init(&ideal_sharp_e, dim);
  List ideals_0;
  list_init(&ideals_0, sizeof(Ideal), ideal_free);

  printf("[[e]]#: ");
  sharp(&ideal_sharp_e, &merge_poly, input_e, &ideals_0);

  ideal_print(&ideal_sharp_e);
  newline();

  Ideal Ix1;
  ideal_init(&Ix1, dim);
  Ix(&Ix1, &ideal_sharp_e, &merge_poly, 0);

  printf("I(x_1): ");
  ideal_print(&Ix1);
  newline();

  Ideal Ix2;
  ideal_init(&Ix2, dim);
  Ix(&Ix2, &ideal_sharp_e, &merge_poly, 1);

  printf("I(x_2): ");
  ideal_print(&Ix2);
  newline();

  Ideal Ix3;
  ideal_init(&Ix3, dim);
  Ix(&Ix3, &ideal_sharp_e, &merge_poly, 2);

  printf("I(x_3): ");
  ideal_print(&Ix3);
  newline();

  list_free(&ydts);
  ydt_poly_free(&ydt_poly);
  ydt_free(&merge);
  ydt_poly_free(&merge_poly);
  ideal_free(&ideal_sharp_f);
  list_free(&ideals_3);
  ideal_free(&ideal_sharp_e);
  list_free(&ideals_0);
  ideal_free(&Ix1);
  ideal_free(&Ix2);
  ideal_free(&Ix3);
  printf("<<<<<<<<<< test_sharp <<<<<<<<<<\n");
}

void test_I_dp(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_I_dp >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);

  YdtPoly ydt_poly;
  ydt_poly_init(&ydt_poly);

  ydt_print((Ydt*) ydts.head->data);

  ydt_to_ydt_poly(&ydt_poly, (Ydt*) ydts.head->data);

  ydt_poly_print(&ydt_poly);

  Ydt merge;
  ydt_init(&merge);
  Ydt* ydt1 = (Ydt*) ydts.head->data;
  Ydt* ydt2 = (Ydt*) ydts.head->next->data;
  ydt_merge(&merge, ydt1, ydt2);

  ydt_print(&merge);

  YdtPoly merge_poly;
  ydt_poly_init(&merge_poly);
  ydt_to_ydt_poly(&merge_poly, &merge);
  ydt_poly_print(&merge_poly);

  int dim = dimension(merge_poly.states_size, merge_poly.max_rank);
  Ideal I_1p;
  ideal_init(&I_1p, dim);
  Ideal one;
  ideal_init(&one, dim);
  ideal_one(&one);
  I_dp(&I_1p, &merge_poly, &one);

  printf("I_1p: ");
  ideal_print(&I_1p);
  newline();

  printf("I_2p: ");
  Ideal I_2p;
  ideal_init(&I_2p, dim);
  I_dp(&I_2p, &merge_poly, &I_1p);

  ideal_print(&I_2p);
  newline();

  /*
  Poly zq0_zqp0;
  poly_init(&zq0_zqp0, I_2p.n);
  Term zq0, zqp0;
  term_init(&zq0, I_2p.n);
  term_init(&zqp0, I_2p.n);
  term_set_ui(&zq0, 1);
  term_set_ui(&zqp0, 1);
  zq0.mi[19] = 1;
  zqp0.mi[23] = 1;

  poly_sub_t(&zq0_zqp0, &zqp0);
  poly_add_t(&zq0_zqp0, &zq0);

  printf("I_2p contains z_q0 - zqp0: %d\n", ideal_contain(&I_2p, &zq0_zqp0));

  poly_free(&zq0_zqp0);
  term_free(&zq0);
  term_free(&zqp0);
  */

  list_free(&ydts);
  ydt_poly_free(&ydt_poly);
  ydt_free(&merge);
  ydt_poly_free(&merge_poly);
  ideal_free(&I_1p);
  ideal_free(&one);
  printf("<<<<<<<<<< test_I_dp <<<<<<<<<<\n");
}

bool list_print_int(void* data) {
  printf("%d\n", *(int*) data);

  return true;
}

int int_cmp(void* data1, void* data2) {
  int left = *(int*) data1;
  int right = *(int*) data2;
  if(left > right) {
    return 1;
  } else if(left < right) {
    return -1;
  } else {
    return 0;
  }
}

void test_list_sort() {
  printf(">>>>>>>>>> test_list_sort >>>>>>>>>>\n");
  List list;
  list_init(&list, sizeof(int), NULL);
  int values[10] = { 2, 6, 4, 5, 9, 1, 7, 10, 3, 8 };

  for(int i = 0; i < 10; i++) {
    list_cons(&values[i], &list);
  }

  printf("before sort: \n");
  list_foreach(&list, list_print_int);

  list_sort_desc(&list, int_cmp);

  printf("after sort: \n");
  list_foreach(&list, list_print_int);

  printf("<<<<<<<<<< test_list_sort <<<<<<<<<<\n");
}

void test_poly_divide() {
  printf(">>>>>>>>>> test_poly_divide >>>>>>>>>>\n");
  Quorem quorem;
  quorem_init(&quorem, 2);
  Poly dividend, divisor;
  poly_init(&dividend, 2);
  poly_init(&divisor, 2);

  Term x2y, xy2, y2, xy;
  term_init(&x2y, 2);
  term_init(&xy2, 2);
  term_init(&y2, 2);
  term_init(&xy, 2);
  term_set_ui(&x2y, 1);
  term_set_ui(&xy2, 1);
  term_set_ui(&y2, 1);
  term_set_ui(&xy, 8);
  x2y.mi[0] = 2;
  x2y.mi[1] = 1;
  xy2.mi[0] = 1;
  xy2.mi[1] = 2;
  y2.mi[0] = 0;
  y2.mi[1] = 2;
  xy.mi[0] = 1;
  xy.mi[1] = 1;

  poly_add_t(&dividend, &x2y);
  poly_add_t(&dividend, &xy2);
  poly_add_t(&dividend, &y2);
  list_sort_desc(&dividend.terms, term_lex_cmp);

  poly_add_t(&divisor, &xy);
  poly_sub_ui(&divisor, 1);
  list_sort_desc(&divisor.terms, term_lex_cmp);

  poly_divide(&quorem, &dividend, &divisor);

  printf("dividend: ");
  poly_print(&dividend);
  newline();
  printf("divisor: ");
  poly_print(&divisor);
  newline();
  printf("normalizer: ");
  mpz_out_str(stdout, 10, quorem.normalizer);
  newline();
  printf("quotient: ");
  poly_print(&quorem.quotient);
  newline();
  printf("remainder: ");
  poly_print(&quorem.remainder);
  newline();

  quorem_free(&quorem);
  poly_free(&dividend);
  poly_free(&divisor);
  term_free(&x2y);
  term_free(&xy2);
  term_free(&y2);
  term_free(&xy);
  printf("<<<<<<<<<< test_poly_divide <<<<<<<<<<\n");
}

void test_ideal_divide() {
  printf(">>>>>>>>>> test_ideal_divide >>>>>>>>>>\n");
  List quorems;
  list_init(&quorems, sizeof(Quorem), quorem_free);
  Ideal divisors;
  ideal_init(&divisors, 2);

  Poly f, f1, f2;
  poly_init(&f, 2);
  poly_init(&f1, 2);
  poly_init(&f2, 2);

  Term xy, y2, xy2, x;
  term_init(&xy, 2);
  term_init(&y2, 2);
  term_init(&xy2, 2);
  term_init(&x, 2);
  term_set_ui(&xy, 1);
  term_set_ui(&y2, 1);
  term_set_ui(&xy2, 1);
  term_set_ui(&x, 1);
  xy.mi[0] = 1;
  xy.mi[1] = 1;
  y2.mi[0] = 0;
  y2.mi[1] = 2;
  xy2.mi[0] = 1;
  xy2.mi[1] = 2;
  x.mi[0] = 1;
  x.mi[1] = 0;

  poly_add_t(&f, &xy2);
  poly_sub_t(&f, &x);
  poly_add_t(&f1, &xy);
  poly_add_ui(&f1, 1);
  poly_add_t(&f2, &y2);
  poly_sub_ui(&f2, 1);

  list_sort_desc(&f.terms, term_lex_cmp);
  list_sort_desc(&f1.terms, term_lex_cmp);
  list_sort_desc(&f2.terms, term_lex_cmp);

  list_cons(&f1, &divisors.base);
  list_cons(&f2, &divisors.base);

  // list_reverse(&divisors.base);

  ideal_divide(&quorems, &f, &divisors);

  printf("dividend: ");
  poly_print(&f);
  newline();
  printf("divisors: ");
  ideal_print(&divisors);
  newline();
  printf("quotient and remainder: \n");
  ListNode* p = quorems.head;
  while(p != NULL) {
    Quorem* quorem = (Quorem*) p->data;
    printf("normalizer: ");
    mpz_out_str(stdout, 10, quorem->normalizer);
    newline();
    printf("quotient: ");
    poly_print(&quorem->quotient);
    newline();
    printf("remainder: ");
    poly_print(&quorem->remainder);
    newline();
    p = p->next;
  }

  list_free(&quorems);
  poly_free(&divisors);
  poly_free(&f);
  term_free(&xy);
  term_free(&y2);
  term_free(&xy2);
  term_free(&x);
  printf("<<<<<<<<<< test_ideal_divide <<<<<<<<<<\n");
}

void test_I_pd(List* type_list, List* func_list_list) {
  printf(">>>>>>>>>> test_I_pd >>>>>>>>>>\n");
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, type_list, func_list_list);

  YdtPoly ydt_poly;
  ydt_poly_init(&ydt_poly);

  ydt_print((Ydt*) ydts.head->data);

  ydt_to_ydt_poly(&ydt_poly, (Ydt*) ydts.head->data);

  ydt_poly_print(&ydt_poly);

  Ydt merge;
  ydt_init(&merge);
  Ydt* ydt1 = (Ydt*) ydts.head->data;
  Ydt* ydt2 = (Ydt*) ydts.head->next->data;
  ydt_merge(&merge, ydt1, ydt2);

  ydt_print(&merge);

  YdtPoly merge_poly;
  ydt_poly_init(&merge_poly);
  ydt_to_ydt_poly(&merge_poly, &merge);
  ydt_poly_print(&merge_poly);

  int dim = dimension(merge_poly.states_size, merge_poly.max_rank);
  Ideal I_p1;
  ideal_init(&I_p1, dim);

  I_pd(&I_p1, &merge_poly, 3);

  ideal_print(&I_p1);
  newline();

  Poly zq0_zqp0;
  poly_init(&zq0_zqp0, I_p1.n);
  Term zq0, zqp0;
  term_init(&zq0, I_p1.n);
  term_init(&zqp0, I_p1.n);
  term_set_ui(&zq0, 1);
  term_set_ui(&zqp0, 1);
  zq0.mi[13] = 1;
  zqp0.mi[17] = 1;

  poly_sub_t(&zq0_zqp0, &zqp0);
  poly_add_t(&zq0_zqp0, &zq0);

  printf("I_p1 contains z_q0 - zqp0: %d\n", ideal_contain(&I_p1, &zq0_zqp0));

  poly_free(&zq0_zqp0);
  term_free(&zq0);
  term_free(&zqp0);

  list_free(&ydts);
  ydt_poly_free(&ydt_poly);
  ydt_free(&merge);
  ydt_poly_free(&merge_poly);
  ideal_free(&I_p1);
  printf("<<<<<<<<<< test_I_pd <<<<<<<<<<\n");
}
