#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "list.h"

#define string_copy(str)                                        \
  strcpy((char*) malloc(sizeof(char) * (strlen(str) + 1)), str)

#define newline()  { puts(""); }

void string_free(void* data);
bool string_print_itr(void* data);
void* list_copy_string(void* data);
int string_cmp(void* data1, void* data2);

void list_copy(List* dst, List* src, CopyFunc copy_func);
void list_print_string(List* list);

int digit(int num);

typedef unsigned long int uli;

uli factorial(int n, int r);
uli combination(int n, int r);
uli multi_choose(int n, int r);

#endif
