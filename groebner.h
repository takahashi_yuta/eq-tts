#ifndef _GROEBNER_H_
#define _GROEBNER_H_

#include <stdbool.h>

#include "poly.h"
#include "ideal.h"

void ideal_groebner(Ideal* groebner, Ideal* ideal, int k);

// グレブナ基底も割られる多項式もソート済みであることを想定している．
bool ideal_contain(Ideal* groebner, Poly* poly);

void ideal_groebner_d(Ideal* groebner_d, Ideal* ideal, int k, int d);

#endif
