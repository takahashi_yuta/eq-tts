#ifndef _LIST_H_
#define _LIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef void  (*FreeFunc)(void*);
typedef bool  (*Iterator)(void*);
typedef void* (*CopyFunc)(void*);
typedef int   (*CmpFunc)(void*, void*);

typedef struct list_node_t {
  void*               data;
  struct list_node_t* next;
} ListNode;

typedef struct list_t {
  int           length;
  size_t        data_size;
  ListNode*     head;
  FreeFunc      free_func;
} List;

void  list_init(List* list, size_t data_size, FreeFunc free_func);
void  list_free(List* list);
void  list_cons(void* data, List* list);
void  list_append(List* left, List* right);
void  list_remove(List *list, void* data);
bool  list_empty(List* list);
int   list_size(List* list);
void  list_foreach(List* list, Iterator iterator);
void  list_reverse(List* list);
void  list_copy(List* dst, List* src, CopyFunc copy_func);
bool  list_contain(List* list, void* data, CmpFunc cmp_func);
void* list_find(List* list, void* data, CmpFunc cmp_func);
void* list_max(List* list, CmpFunc cmp_func);
int   list_index_at(List* list, void* data, CmpFunc cmp_func);
bool  list_including(List* left, List* right, CmpFunc cmp_func);
bool  list_equal_as_set(List* left, List* right, CmpFunc cmp_func);

void  list_sort_desc(List* list, CmpFunc cmp_func);

#endif

