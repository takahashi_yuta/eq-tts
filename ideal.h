#ifndef _IDEAL_H_
#define _IDEAL_H_

#include "list.h"

typedef struct {
  int  n;
  List base;
} Ideal;

void ideal_init(Ideal* ideal, int n);
void ideal_free(void* data);
void ideal_print(void* data);

void ideal_one(Ideal* ideal);

void ideal_mul_t(Ideal* tI, Ideal* I, Term* t);
void ideal_mul_p(Ideal* pI, Ideal* I, Poly* poly);

void ideal_union(Ideal* I_union_J, Ideal* I, Ideal* J);
void ideal_intersect(Ideal* I_inter_J, Ideal* I, Ideal* J);
void ideal_product(Ideal* I_prod_J, Ideal* I, Ideal* J);

void ideal_eliminate_k(Ideal* ideal, int k);

// イデアルの基底がグレブナ基底であるならば，イデアルの所属を判定できる．
// すべて項は整列済みであることを仮定．
void ideal_divide(List* quorems, Poly* dividient, Ideal* divisors);

#endif
