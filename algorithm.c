#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "util.h"
#include "poly.h"
#include "ideal.h"
#include "groebner.h"
#include "ydt_to_poly.h"

#include "algorithm.h"

void Ix(Ideal* I_x, Ideal* I_z, YdtPoly* ydt, int i) {
  int z_base = z_base(ydt->states_size, ydt->max_rank);

  ListNode* pp = I_z->base.head;
  while(pp != NULL) {
    Poly* poly_z = (Poly*) pp->data;
    Poly poly_x;
    poly_init(&poly_x, poly_z->n);

    ListNode* tp = poly_z->terms.head;
    while(tp != NULL) {
      Term* term_z = (Term*) tp->data;
      Term term_x;
      term_init(&term_x, term_z->n);
      term_set_mpz(&term_x, term_z->coef);

      for(int index_z = z_base; index_z < term_z->n; index_z++) {
        int index_x = z_to_x(ydt->max_rank, z_base, index_z, i);
        term_x.mi[index_x] = term_z->mi[index_z];
      }

      poly_add_t(&poly_x, &term_x);

      term_free(&term_x);
      tp = tp->next;
    }

    list_cons(&poly_x, &I_x->base);

    pp = pp->next;
  }
}

void Ixs(Ideal* Ixs, YdtPoly* ydt, List* ideals) {
  int i = 0;

  ListNode* ip = ideals->head;
  while(ip != NULL) {
    Ideal* ideal_z = (Ideal*) ip->data;
    Ideal ideal_x;
    ideal_init(&ideal_x, ideal_z->n);

    Ix(&ideal_x, ideal_z, ydt, i);

    list_append(&Ixs->base, &ideal_x.base);

    i++;
    ip = ip->next;
  }
}

void sharp(Ideal* ideal_sharp, YdtPoly* ydt, YdtInput* input, List* ideals) {
  if(input->rank != ideals->length) {
    printf("f^r in Σ, [[f]]#(I_1,...,I_k): r != k\n");
    exit(1);
  }

  Ideal ideal_z_r;
  ideal_init(&ideal_z_r, ideal_sharp->n);

  int z_base = z_base(ydt->states_size, ydt->max_rank);

  ListNode* rp = ydt->rules.head;
  while(rp != NULL) {
    PolyRule* rule = (PolyRule*) rp->data;

    if(ydt_input_cmp(input, &rule->input) == 0) {
      int j = list_index_at(&ydt->states, &rule->state, string_cmp);
      for(int k = 0; k < 2; k++) {
        Poly r = rule->poly[k];
        Poly z_r;
        poly_init(&z_r, r.n);
        Term z;
        term_init(&z, r.n);
        term_set_ui(&z, 1);
        z.mi[z_jk(z_base, j, k)] = 1;

        poly_add_t(&z_r, &z);
        poly_sub_p(&z_r, &r);

        list_cons(&z_r, &ideal_z_r.base);

        term_free(&z);
      }
    }

    rp = rp->next;
  }

  Ideal ideal_xs;
  ideal_init(&ideal_xs, ideal_sharp->n);
  Ixs(&ideal_xs, ydt, ideals);

  Ideal ideal_z_r_x;
  ideal_init(&ideal_z_r_x, ideal_sharp->n);
  ideal_union(&ideal_z_r_x, &ideal_z_r, &ideal_xs);
  // ideal_product(&ideal_z_r_x, &ideal_z_r, &ideal_xs);

#if defined(DEBUG_ALGO) || defined(DEBUG)
  printf("===== before groebner for sharp =====\n");
  ideal_print(&ideal_z_r_x);
  printf("\n");
#endif

  struct rusage usage;
  struct timeval ut1, ut2;
  struct timeval st1, st2;
  struct timeval s, e;

  printf("[==================== calculate [[%s]]# =====================]\n", input->symbol);
  gettimeofday(&s, NULL);
  getrusage(RUSAGE_SELF, &usage);
  ut1 = usage.ru_utime;
  st1 = usage.ru_stime;
  
  ideal_groebner(ideal_sharp, &ideal_z_r_x, z_base);

  printf("ideal: ");
  ideal_print(ideal_sharp);
  printf("\n");

  getrusage(RUSAGE_SELF, &usage);
  ut2 = usage.ru_utime;
  st2 = usage.ru_stime;
  gettimeofday(&e, NULL);

  printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
  printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
  printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
  printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);
  printf("[===========================================================]\n");

  ideal_eliminate_k(ideal_sharp, z_base);

  ideal_free(&ideal_z_r);
  ideal_free(&ideal_xs);
  ideal_free(&ideal_z_r_x);
}

void I_dp(Ideal* ideal_dp, YdtPoly* ydt, Ideal* ideal) {
  Ideal intersect;
  ideal_init(&intersect, ideal_dp->n);
  ideal_one(&intersect);

  ListNode* ip = ydt->inputs.head;
  while(ip != NULL) {
    YdtInput* input = (YdtInput*) ip->data;
    List ideals;
    list_init(&ideals, sizeof(Ideal), NULL);

    for(int i = 0; i < input->rank; i++) {
      list_cons(ideal, &ideals);
    }

    Ideal ideal_sharp;
    ideal_init(&ideal_sharp, ideal_dp->n);
    sharp(&ideal_sharp, ydt, input, &ideals);

    list_free(&ideals);

    struct rusage usage;
    struct timeval ut1, ut2;
    struct timeval st1, st2;
    struct timeval s, e;

    printf("[==================== calculate intersect ====================]\n");
    
    gettimeofday(&s, NULL);
    getrusage(RUSAGE_SELF, &usage);
    ut1 = usage.ru_utime;
    st1 = usage.ru_stime;

    ideal_intersect(ideal_dp, &intersect, &ideal_sharp);

    printf("ideal: ");
    ideal_print(ideal_dp);
    printf("\n");

    getrusage(RUSAGE_SELF, &usage);
    ut2 = usage.ru_utime;
    st2 = usage.ru_stime;
    gettimeofday(&e, NULL);

    printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
    printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);
    printf("[=============================================================]\n");

    ideal_free(&intersect);
    ideal_init(&intersect, ideal_dp->n);
    intersect.base = ideal_dp->base;

    ideal_init(ideal_dp, ideal_dp->n);

    ip = ip->next;
  }

  ideal_dp->n = intersect.n;
  ideal_dp->base = intersect.base;
}

//////////////////////////////////////////////////

void I_pd(Ideal* ideal_pd, YdtPoly* ydt, int d) {
  Ideal ideal;
  ideal_init(&ideal, ideal_pd->n);
  ideal_one(&ideal);

  int x_dim = z_base(ydt->states_size, ydt->max_rank);

  bool done;
  do {
    done = true;
    ListNode* ip = ydt->inputs.head;
    while(ip != NULL) {
      YdtInput* input = (YdtInput*) ip->data;
      List ideals;
      list_init(&ideals, sizeof(Ideal), NULL);

      for(int i = 0; i < input->rank; i++) {
        list_cons(&ideal, &ideals);
      }

      Ideal ideal_sharp;
      ideal_init(&ideal_sharp, ideal_pd->n);
      sharp(&ideal_sharp, ydt, input, &ideals);

      struct rusage usage;
      struct timeval ut1, ut2;
      struct timeval st1, st2;
      struct timeval s, e;

      printf("[==================== calculate a_d ==========================]\n");

      gettimeofday(&s, NULL);
      getrusage(RUSAGE_SELF, &usage);
      ut1 = usage.ru_utime;
      st1 = usage.ru_stime;

      Ideal ideal_d;
      ideal_init(&ideal_d, ideal_sharp.n);
      ideal_groebner_d(&ideal_d, &ideal_sharp, x_dim, d);

      getrusage(RUSAGE_SELF, &usage);
      ut2 = usage.ru_utime;
      st2 = usage.ru_stime;
      gettimeofday(&e, NULL);

      printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
      printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);

      printf("[=============================================================]\n");

      printf("[==================== calculate intersect ====================]\n");

      gettimeofday(&s, NULL);
      getrusage(RUSAGE_SELF, &usage);
      ut1 = usage.ru_utime;
      st1 = usage.ru_stime;

      Ideal intersect;
      ideal_init(&intersect, ideal.n);
      ideal_intersect(&intersect, &ideal, &ideal_d);

      getrusage(RUSAGE_SELF, &usage);
      ut2 = usage.ru_utime;
      st2 = usage.ru_stime;
      gettimeofday(&e, NULL);

      printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
      printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);

      printf("[=============================================================]\n");

      printf("[============= calculate a_d after intersection ==============]\n");

      gettimeofday(&s, NULL);
      getrusage(RUSAGE_SELF, &usage);
      ut1 = usage.ru_utime;
      st1 = usage.ru_stime;

      Ideal groebner_d;
      ideal_init(&groebner_d, ideal_sharp.n);
      ideal_groebner_d(&groebner_d, &intersect, x_dim, d);

      printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
      printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
      printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);

      printf("[=============================================================]\n");

      if(!list_equal_as_set(&ideal.base, &groebner_d.base, poly_cmp)) {
        ideal_free(&ideal);
        ideal = groebner_d;

        done = false;
        break;
      }

      ideal_free(&ideal_sharp);
      ideal_free(&ideal_d);

      ip = ip->next;
    }
  } while(!done);

  *ideal_pd = ideal;
}
