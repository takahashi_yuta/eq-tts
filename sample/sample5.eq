type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ q1(x2) ^ "b" ^ q(x2)
  | E              -> "a" ^ "b"
and q1 = function
  | F (x1, x2, x3) -> q2(x3) ^ q2(x2) ^ q1(x2) ^ "b" ^ "a"
  | E              -> "b" ^ "a" 
and q2 = function
  | F (x1, x2, x3) -> "b" ^ "a" ^ q1(x2) ^ q2(x2) ^ q1(x3)
  | E              -> "b" ^ "a"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b"