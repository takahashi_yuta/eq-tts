type input = 
  | F of input * input
  | E

let rec q = function
  | F (x2, x3) -> q2(x3) ^ "a" ^ q1(x2) ^ "b"
  | E              -> "a" ^ "b"
and q1 = function
  | F(x2, x3)  -> q1(x3) ^ q1(x2) ^ "b" ^ "a"
  | E              -> "b" ^ "a" 
and q2 = function
  | F(x2, x3)  -> "a" ^ q1(x2) ^ "b" ^ q(x3)
  | E              -> "a" ^ "b"
  
let rec qp = function
  | F(x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b"