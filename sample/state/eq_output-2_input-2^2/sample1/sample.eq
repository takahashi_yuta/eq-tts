type input = 
  | F of input * input
  | E

let rec q = function
  | F (x2, x3) -> q(x3) ^ "a" ^ "b" ^ q(x2)
  | E              -> "a" ^ "b"
  
let rec qp = function
  | F(x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b"