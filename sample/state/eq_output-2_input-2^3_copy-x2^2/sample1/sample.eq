type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x2) ^ "a" ^ "b" ^ q(x2)
  | E              -> "a" ^ "b"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x2) 
  | E             -> "a" ^ "b"
