type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q2(x3) ^ "a" ^ q1(x2) ^ "b" ^ "c"
  | E              -> "a" ^ "b" ^ "c"
and q1 = function
  | F(x1, x2, x3)  -> q1(x3) ^ q1(x2) ^ "b" ^ "c" ^ "a"
  | E              -> "b" ^ "c" ^ "a" 
and q2 = function
  | F(x1, x2, x3)  -> "a" ^ q1(x2) ^ "b" ^ "c" ^ q(x3)
  | E              -> "a" ^ "b" ^ "c"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ "c" ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b" ^ "c"
