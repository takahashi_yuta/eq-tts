type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ "b" ^ "c" ^ q(x2)
  | E              -> "a" ^ "b" ^ "c"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ "c" ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b" ^ "c"