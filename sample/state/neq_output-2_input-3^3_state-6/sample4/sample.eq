type input = 
  | F of input * input * input
  | G of input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q2(x3) ^ "a" ^ q1(x2) ^ "b"
  | G (x1, x2)     -> "a" ^ q1(x1) ^ "b" ^ q2(x1)
  | E              -> "a" ^ "b"
and q1 = function
  | F (x1, x2, x3) -> q1(x3) ^ q1(x2) ^ "b" ^ "a"
  | G (x1, x2)     -> "b" ^ q(x1) ^ "a" ^ q1(x1)
  | E              -> "b" ^ "a"
and q2 = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ q1(x2) ^ "b"
  | G (x1, x2)     -> "a" ^ q3(x1) ^ "b" ^ q(x1)
  | E              -> "a" ^ "b"
and q3 = function
  | F (x1, x2, x3) -> q1(x3) ^ "a" ^ "a" ^ q3(x2)
  | G (x1, x2)     -> "b" ^ q(x1) ^ "a" ^ q1(x1)
  | E              -> "b" ^ "a"
and q4 = function
  | F (x1, x2, x3) -> "a" ^ q1(x3) ^ "b" ^ q2(x2)
  | G (x1, x2)     -> q(x1) ^ "a" ^ "b" ^ q2(x1)
  | E              -> "a" ^ "b"
and q5 = function
  | F (x1, x2, x3) -> "b" ^ q(x3) ^ "a" ^ q3(x2)
  | G (x1, x2)     -> q3(x1) ^ "b" ^ "a" ^ q5(x1)
  | E              -> "b" ^ "a"
  
let rec qp = function
  | F (x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x3)
  | G (x1, x2)     -> qp(x1) ^ "a" ^ "b" ^ qp(x1)
  | E              -> "a" ^ "b"

