type input = 
  | F of input * input * input
  | G of input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ "b" ^ q(x2)
  | G (x1, x2)     -> q(x1) ^ q(x1) ^ "a" ^ "b"
  | E              -> "a" ^ "b"
and qp = function
  | F (x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ q(x3) 
  | G (x1, x2)     -> "a" ^ "b" ^ q(x1) ^ qp(x1)
  | E              -> "a" ^ "b"
