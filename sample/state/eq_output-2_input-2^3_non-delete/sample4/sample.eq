type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q2(x3) ^ "a" ^ q1(x2) ^ "b" ^ q(x1)
  | E              -> "a" ^ "b"
and q1 = function
  | F(x1, x2, x3)  -> q1(x1) ^ q1(x3) ^ q1(x2) ^ "b" ^ "a"
  | E              -> "b" ^ "a" 
and q2 = function
  | F(x1, x2, x3)  -> "a" ^ q3(x2) ^ "b" ^ q(x3) ^ q2(x1)
  | E              -> "a" ^ "b"
and q3 = function
  | F(x1, x2, x3)  -> q3(x3) ^ "b" ^ q(x1) ^ "a" ^ q1(x2)
  | E              -> "b" ^ "a"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x1) ^ qp(x3) ^ qp(x2)
  | E             -> "a" ^ "b"
