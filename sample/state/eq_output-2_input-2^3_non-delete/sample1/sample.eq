type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ "b" ^ q(x2) ^ q(x1)
  | E              -> "a" ^ "b"
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x1) ^ qp(x3) ^ qp(x2)
  | E             -> "a" ^ "b"
