type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ q1(x2) ^ "b" ^ q(x2) ^ q(x1)
  | E              -> "a" ^ "b"
and q1 = function
  | F(x1, x2, x3)  -> q1(x1) ^ q1(x3) ^ q1(x2) ^ q1(x2) ^ "b" ^ "a"
  | E              -> "b" ^ "a" 
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x2) ^ qp(x3) ^ qp(x1)
  | E             -> "a" ^ "b"