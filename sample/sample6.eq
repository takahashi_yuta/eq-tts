type input = 
  | F of input * input * input
  | G of input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> "a" ^ q1(x1) ^ "b" ^ q(x2) ^ q(x1)
  | G (x1, x2)     -> "a" ^ q1(x1) ^ "b" ^ q2(x1)
  | E              -> "a" ^ "b"
and q1 = function
  | F (x1, x2, x3) -> q1(x1) ^ "b" ^ "a" ^ q1(x2) ^ q1(x1)
  | G (x1, x2)     -> "b" ^ q2(x1) ^ "a" ^ q2(x1)
  | E              -> "b" ^ "a" 
and q2 = function
  | F (x1, x2, x3) -> q(x1) ^ "a" ^ "b" ^ q(x2) ^ q(x1)
  | G (x1, x2)     -> q(x1) ^ "a" ^ q1(x1) ^ "b"
  | E              -> "a" ^ "b"
  
let rec qp = function
  | F (x1, x2, x3) -> "a" ^ "b" ^ qp(x1) ^ qp(x1) ^ qp(x2)
  | G (x1, x2)     -> qp(x1) ^ "a" ^ "b" ^ qp(x1)
  | E              -> "a" ^ "b"