type input = 
  | F of input * input * input
  | G of input * input
  | H of input
  | E

let rec q4 = function
  | F (x1, x2, x3) -> q0(x1) ^ q4(x2) ^ q0(x1) ^ "a" ^ "b" ^ q0(x3)
  | G (x1, x2)     -> "a" ^ q1(x2) ^ "b" ^ q2(x1)
  | H (x1)         -> "a" ^ "b" ^ qp2(x1)
  | E              -> "a" ^ "b"
and q0 = function
  | F (x1, x2, x3) -> q0(x3) ^ "a" ^ q1(x2) ^ "b" ^ q0(x2)
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "a" ^ "b"
and q1 = function
  | F (x1, x2, x3) -> q1(x3) ^ q1(x2) ^ q1(x2) ^ "b" ^ "a"
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "b" ^ "a"
and qp0 = function
  | F (x1, x2, x3) -> "a" ^ "b" ^ qp0(x2) ^ qp0(x2) ^ qp0(x3)
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "a" ^ "b"
and q2 = function
  | F (x1, x2, x3) -> q2(x3) ^ "a" ^ q3(x2) ^ "b" ^ q2(x2) ^ q2(x1)
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "a" ^ "b"
and q3 = function
  | F (x1, x2, x3)  -> q3(x1) ^ q3(x3) ^ q3(x2) ^ q3(x2) ^ "b" ^ "a"
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "b" ^ "a" 
and qp2 = function
  | F (x1, x2, x3) -> "a" ^ "b" ^ qp2(x2) ^ qp2(x2) ^ qp2(x3) ^ qp2(x1)
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E             -> "a" ^ "b"
and qp3 = function
  | F (x1, x2, x3) -> qp3(x2) ^ qp0(x1) ^ "a" ^ q1(x3) ^ "b" ^ q0(x1)
  | G (x1, x2)     -> qp2(x1) ^ "a" ^ "b" ^ q0(x2)
  | H (x1)         -> q0(x1) ^ "a" ^ "b"
  | E              -> "a" ^ "b"
  
let rec qp3 = function
  | F (x1, x2, x3) -> ""
  | G (x1, x2)     -> ""
  | H (x1)         -> ""
  | E              -> "a" ^ "b"
