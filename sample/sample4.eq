type input = 
  | Frame of input * input
  | End
  | Defs of input * input
  | Height of input
  | Width of input
  | Button of input
  | Message
  | Height20
  | Width50

let rec q = function
  | Frame (x1, x2) -> "<" ^ "frame" ^ q1(x1) ^ q(x2) ^ "<" ^ "/" ^ "frame" ^ ">"
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> "<" ^ "button" ^ ">" ^ q3(x1) ^ "<" ^ "/" ^ "button" ^ ">"
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q1 = function
  | Frame (x1, x2) -> ""
  | End            -> ">"
  | Defs (x1, x2)  -> q2(x1) ^ q1(x2)
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> ""
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q2 = function
  | Frame (x1, x2) -> ""
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> " " ^ "height" ^ "=" ^ "'" ^ q3(x1) ^ "'"
  | Width (x1)     -> " " ^ "width" ^ "=" ^ "'" ^ q3(x1) ^ "'"
  | Button (x1)    -> ""
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q3 = function
  | Frame (x1, x2) -> ""
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> ""
  | Message        -> "Do not press!"
  | Height20       -> "20"
  | Width50        -> "50"
  
let rec qp = function
  | Frame (x1, x2) -> "<" ^ "frame" ^ q1(x1) ^ ">" ^ qp(x2) ^ "<" ^ "/" ^ "frame" ^ ">"
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> "<" ^ "button" ^ ">" ^ q3(x1) ^ "<" ^ "/" ^ "button" ^ ">"
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q1 = function
  | Frame (x1, x2) -> ""
  | End            -> ""
  | Defs (x1, x2)  -> q2(x1) ^ q1(x2)
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> ""
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q2 = function
  | Frame (x1, x2) -> ""
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> " " ^ "height" ^ "=" ^ "'" ^ q3(x1) ^ "'"
  | Width (x1)     -> " " ^ "width" ^ "=" ^ "'" ^ q3(x1) ^ "'"
  | Button (x1)    -> ""
  | Message        -> ""
  | Height20       -> ""
  | Width50        -> ""
and q3 = function
  | Frame (x1, x2) -> ""
  | End            -> ""
  | Defs (x1, x2)  -> ""
  | Height (x1)    -> ""
  | Width (x1)     -> ""
  | Button (x1)    -> ""
  | Message        -> "Do not press!"
  | Height20       -> "20"
  | Width50        -> "50"
