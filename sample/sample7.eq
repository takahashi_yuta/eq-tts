type input =
  | Projects of input * input
  | Project of input * input
  | Keyword of input * input
  | Member of input * input
  | End
  | Alice
  | Bob
  | Carol
  | Dave

let rec q = function
  | Projects (x1, x2) -> "<list>" ^ q1(x1) ^ "</list>" ^ q(x2) ^ "<list>" 
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and q1 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> "<item>" ^ q2(x1) ^ "</item>"
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and q2 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> "<member>" ^ q3(x1) ^ "</member>" ^ q2(x2)
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and q3 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> "Alice"
  | Bob               -> "Bob"
  | Carol             -> "Carol"
  | Dave              -> "Dave"

let rec qp = function
  | Projects (x1, x2) -> "<list>" ^ qp1(x1) ^ qp2(x2) ^ qp(x2)
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and qp1 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> "<item>" ^ qp3(x1) ^ qp4(x2) ^ qp1(x2)
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and qp2 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> qp2(x2)
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> "</list>"
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and qp3 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> "<member>" ^ qp5(x1) ^ qp6(x2) ^ qp3(x2)
  | End               -> ""
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and qp4 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> qp4(x2)
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> "</item>"
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""
and qp5 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> ""
  | End               -> ""
  | Alice             -> "Alice"
  | Bob               -> "Bob"
  | Carol             -> "Carol"
  | Dave              -> "Dave"
and qp6 = function
  | Projects (x1, x2) -> ""
  | Project (x1, x2)  -> ""
  | Keyword (x1, x2)  -> ""
  | Member (x1, x2)   -> qp6(x2)
  | End               -> "</member>"
  | Alice             -> ""
  | Bob               -> ""
  | Carol             -> ""
  | Dave              -> ""