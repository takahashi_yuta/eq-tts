```ocaml
type input = 
  | F of input * input * input
  | E

let rec q = function
  | F (x1, x2, x3) -> q(x3) ^ "a" ^ q1(x2) ^ "b" ^ q(x2)
  | E              -> "a" ^ "b"
and q1 = function
  | F(x1, x2, x3)  -> q1(x3) ^ q1(x2) ^ q1(x2) ^ "b" ^ "a"
  | E              -> "b" ^ "a" 
  
let rec qp = function
  | F(x1, x2, x3) -> "a" ^ "b" ^ qp(x2) ^ qp(x2) ^ qp(x3) 
  | E             -> "a" ^ "b"
```

```
yDT = (Q, Σ, Δ, q_0, δ)
	Q = {q1, q}
	Σ = {E^(0), F^(3)}
	Δ = {"b", "a"}
	q_0 = q
	δ = {
		q1(E()) → "a" "b" 
		q1(F(x1, x2, x3)) → "a" "b" q1(x2) q1(x2) q1(x3) 
		q(E()) → "b" "a" 
		q(F(x1, x2, x3)) → q(x2) "b" q1(x2) "a" q(x3) 
	}
```

```
yDT = (Q, Σ, Δ, q_0, δ)
	Q = {qp}
	Σ = {E^(0), F^(3)}
	Δ = {"b", "a"}
	q_0 = qp
	δ = {
		qp(E()) → "b" "a" 
		qp(F(x1, x2, x3)) → qp(x3) qp(x2) qp(x2) "b" "a" 
	}
```

合成結果

```
yDT = (Q, Σ, Δ, q_0, δ)
	Q = {q@q, q@q1, qp@qp}
	Σ = {F^(3), E^(0)}
	Δ = {"a", "b"}
	q_0 = (null)
	δ = {
		q@q(F(x1, x2, x3)) → q@q(x3) "a" q@q1(x2) "b" q@q(x2) 
		q@q(E()) → "a" "b" 
		q@q1(F(x1, x2, x3)) → q@q1(x3) q@q1(x2) q@q1(x2) "b" "a" 
		q@q1(E()) → "b" "a" 
		qp@qp(F(x1, x2, x3)) → "a" "b" qp@qp(x2) qp@qp(x2) qp@qp(x3) 
		qp@qp(E()) → "a" "b" 
	}
```

変数の割り当て

```
x_0  = y
x_1  = x_1_q_0
x_2  = x_1_q_1
x_3  = x_2_q_0
x_4  = x_2_q_1
x_5  = x_3_q_0
x_6  = x_3_q_1
x_7  = x_1_q1_0
x_8  = x_1_q1_1
x_9  = x_2_q1_0
x_10 = x_2_q1_1
x_11 = x_3_q1_0
x_12 = x_3_q1_1
x_13 = x_1_qp_0
x_14 = x_1_qp_1
x_15 = x_2_qp_0
x_16 = x_2_qp_1
x_17 = x_3_qp_0
x_18 = x_3_qp_1
x_19 = z_q_0
x_20 = z_q_1
x_21 = z_q1_0
x_22 = z_q1_1
x_23 = z_qp_0
x_24 = z_qp_1
```

inductive invariant
```
<8 x_19 + -7 x_24 + 7, 1 x_20 + -1 x_24, 8 x_21 + -5 x_24 + 5, 1 x_22 + -1 x_24, 8 x_23 + -7 x_24 + 7>
<8 z_q_0 - 7 z_q'_1 + 7, z_q_1 - z_q'_1, 8 z_q1_0 - 5 z_q'_1 + 5, z_q1_1 - z_qp_1, 8 z_qp_0 - 7 z_qp_1 + 7>
```
