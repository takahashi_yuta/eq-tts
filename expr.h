#ifndef _EXPR_H_
#define _EXPR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "list.h"

typedef struct {
  char* const_name;
  List  variables;
} Lhs;

void lhs_free(void* data);

enum RhsElementTag {
  RHS_OUTPUT, RHS_CALL
};

typedef struct {
  char* func_name;
  char* variable;
} RhsCall;

typedef struct {
  enum RhsElementTag tag;
  union {
    char*   output;
    RhsCall rhs_call;
  };
} RhsElement;

void rhs_element_free(void* data);

typedef struct {
  Lhs  lhs;
  List rhs;
} ConstMatch;

void const_match_free(void *data);

typedef struct {
  char* func_name;
  List  const_matches;
} FuncBody;

void func_body_free(void *data);
void func_body_list_free(void* data);

typedef struct {
  char* const_name;
  List  type_exprs;
} TypeConst;

void type_const_free(void* data);

typedef struct {
  char* type_name;
  List  consts;
} Type;

void type_free(void* data);

#endif
