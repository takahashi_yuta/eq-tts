#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "poly.h"
#include "ideal.h"
#include "ydt_to_poly.h"

void sharp(Ideal* ideal_sharp, YdtPoly* ydt, YdtInput* input, List* ideals);

void Ix(Ideal* I_x, Ideal* I_z, YdtPoly* ydt, int i);

// 等価でない証拠を探す半アルゴリズム
// FIXME total 以外にも対応するためには第3引数をかえる必要がある
void I_dp(Ideal* ideal_dp, YdtPoly* ydt, Ideal* ideal);

// 等価な証拠を探す半アルゴリズム

// FIXME total 以外にも対応するためには第3引数をかえる必要がある
void I_pd(Ideal* ideal_pd, YdtPoly* ydt, int d);


#endif
