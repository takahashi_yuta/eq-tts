#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "list.h"
#include "util.h"

void list_init(List* list, size_t data_size, FreeFunc free_func) {
  list->length       = 0;
  list->data_size    = data_size;
  list->head         = NULL;
  list->free_func    = free_func;
}

void list_free(List* list) {
  ListNode* current;

  while(list->head != NULL) {
    current    = list->head;
    list->head = current->next;

    if(list->free_func != NULL) {
      list->free_func(current->data);
    }

    free(current->data);
    free(current);
  }
}

void list_cons(void* data, List* list) {
  ListNode* node = (ListNode*) malloc(sizeof(ListNode));
  node->data = malloc(list->data_size);
  memcpy(node->data, data, list->data_size);

  node->next = list->head;
  list->head = node;

  list->length++;
}

// 左のリストの末尾に右のリストを連結する．
// メモリはコピーされないため，解放のタイミングに注意．
void list_append(List* left, List* right) {
  left->length += right->length;

  if(left->head == NULL) {
    left->head = right->head;
    return;
  }

  ListNode* end = left->head;

  while(end->next != NULL) {
    end = end->next;
  }

  end->next = right->head;
}

void list_remove(List* list, void* data) {
  if(data == NULL) {
    return;
  }

  ListNode* node = list->head;
  ListNode* prev = NULL;

  while(node != NULL && node->data != data) {
    prev = node;
    node = node->next;
  }

  if(prev == NULL) {
    ListNode* head = list->head;
    ListNode* next = head->next;

    list->free_func(head->data);
    free(head);

    list->head = next;
    list->length--;
  } else if(node != NULL) {
    ListNode* next = node->next;

    list->free_func(node->data);
    free(node);

    prev->next = next;

    list->length--;
  }
}

bool list_empty(List* list) {
  return list->head == NULL;
}

int list_size(List* list) {
  return list->length;
}

void list_foreach(List* list, Iterator iterator) {
  ListNode* node = list->head;

  bool result = true;
  while(node != NULL && result) {
    result = iterator(node->data);
    node = node->next;
  }
}

void list_reverse(List* list) {
  ListNode* current = list->head;
  ListNode* next;
  ListNode* prev = NULL;

  while(current != NULL) {
    next = current->next;
    current->next = prev;
    prev = current;
    current = next;
  }

  list->head = prev;
}

void list_copy(List* dst, List* src, CopyFunc copy_func) {
  ListNode* p = src->head;
  while(p != NULL) {
    void* copy = copy_func(p->data);
    list_cons(copy, dst);
    free(copy);
    p = p->next;
  }

  list_reverse(dst);
}

bool list_contain(List* list, void* data, CmpFunc cmp_func) {
  return list_find(list, data, cmp_func) != NULL ? true : false;
}

void* list_find(List* list, void* data, CmpFunc cmp_func) {
  ListNode* node = list->head;
  while(node != NULL) {
    if(cmp_func(data, node->data) == 0) {
      return node->data;
    }
    node = node->next;
  }

  return NULL;
}

void* list_max(List* list, CmpFunc cmp_func) {
  ListNode* node = list->head;
  ListNode* max = list->head;

  while(node != NULL) {
    if(cmp_func(node->data, max->data) > 0) {
      max = node;
    }
    node = node->next;
  }

  return max == NULL ? NULL : max->data;
}

int list_index_at(List* list, void* data, CmpFunc cmp_func) {
  ListNode* node = list->head;
  int index = 0;

  while(node != NULL) {
    if(cmp_func(node->data, data) == 0) {
      return index;
    }
    index++;
    node = node->next;
  }

  return index;
}

// 雑な実装
bool list_including(List* left, List* right, CmpFunc cmp_func) {
  if((left->length == 0) ^ (right->length == 0)) {
    return false;
  }

  ListNode* node = right->head;
  while(node != NULL) {
    if(!list_contain(left, node->data, cmp_func)) {
      return false;
    }
    node = node->next;
  }

  return true;
}

// 雑な実装
bool list_equal_as_set(List* left, List* right, CmpFunc cmp_func) {
  return list_including(left, right, cmp_func) && list_including(right, left, cmp_func);
}

void list_qsort(void* list, int begin, int end, CmpFunc cmp_func) {
  void** array = (void**) list;
  int i = begin;
  int j = end;
  void* pivot;
  void* temp;

  pivot = array[(begin + end)/2];

  while(1) {
    while(cmp_func(array[i], pivot) > 0) { i++; }
    while(cmp_func(array[j], pivot) < 0) { j--; }
    if(i >= j) { break; }

    temp = array[i];
    array[i] = array[j];
    array[j] = temp;

    i++;
    j--;
  }

  if(begin < i-1){
    list_qsort(array, begin, i-1, cmp_func);
  }
  if(j+1 < end){
    list_qsort(array, j+1, end, cmp_func);
  }
}

void list_sort_desc(List* list, CmpFunc cmp_func) {
  if(list->length <= 1) {
    return;
  }

  void** data_list = (void**) malloc(sizeof(void*) * list->length);

  ListNode* node = list->head;
  int index = 0;
  while(node != NULL) {
    data_list[index++] = node->data;
    node = node->next;
  }

  list_qsort(data_list, 0, list->length-1, cmp_func);

  index = 0;
  node = list->head;
  while(node != NULL) {
    node->data = data_list[index];
    index++;
    node = node->next;
  }
}
