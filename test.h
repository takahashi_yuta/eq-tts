#ifndef _TEST_H_
#define _TEST_H_

#include "list.h"

void test_list();
void test_term();
void test_poly();
void test_groebner();
void test_intersect();

void test_expr_to_ydt(List* type_list, List* func_list_list);
void test_ydt_merge(List* type_list, List* func_list_list);
void test_ydt_to_poly(List* type_list, List* func_list_list);

void test_sharp(List* type_list, List* func_list_list);
void test_I_dp(List* type_list, List* func_list_list);

void test_list_sort();

void test_poly_divide();
void test_ideal_divide();

void test_I_pd(List* type_list, List* func_list_list);

#endif
