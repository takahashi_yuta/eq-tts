#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "util.h"
#include "ydt.h"

#include "expr.h"

void ydt_input_set(YdtInput* dst, YdtInput* src) {
  dst->rank = src->rank;
  dst->symbol = strdup(src->symbol);
}

void ydt_input_free(void *data) {
  YdtInput* input = (YdtInput*) data;

  free(input->symbol);
}

int ydt_input_cmp(void *data1, void* data2) {
  YdtInput* input1 = (YdtInput*) data1;
  YdtInput* input2 = (YdtInput*) data2;

  if(input1->rank > input2->rank) {
    return 1;
  }

  if(input1->rank < input2->rank) {
    return -1;
  }

  return strcmp(input1->symbol, input2->symbol);
}

void* list_copy_ydt_input(void* data) {
  YdtInput* input = (YdtInput*) data;
  YdtInput* ret = (YdtInput*) malloc(sizeof(YdtInput));

  ydt_input_set(ret, input);

  return ret;
}

void ydt_lhs_init(YdtLhs *lhs) {
  list_init(&lhs->variables, sizeof(char*), string_free);
}

void ydt_lhs_free(void *data) {
  YdtLhs* lhs = (YdtLhs*) data;

  free(lhs->state);
  ydt_input_free(&lhs->input);
  list_free(&lhs->variables);
}

void ydt_rhs_call_free(void *data) {
  YdtRhsCall* call = (YdtRhsCall*) data;

  free(call->state);
  free(call->variable);
}

void* list_copy_ydt_rhs_element(void* data) {
  YdtRhsElement* elm = (YdtRhsElement*) data;
  YdtRhsElement* ret = (YdtRhsElement*) malloc(sizeof(YdtRhsElement));

  ret->tag = elm->tag;

  switch(elm->tag) {
  case YDT_RHS_OUTPUT:
    ret->output = strdup(elm->output);
    break;
  case YDT_RHS_CALL:
    ret->call.state = strdup(elm->call.state);
    ret->call.variable = strdup(elm->call.variable);
    break;
  }

  return ret;
}

void ydt_rhs_element_free(void* data) {
  YdtRhsElement* element = (YdtRhsElement*) data;
  switch(element->tag) {
  case YDT_RHS_OUTPUT:
    free(element->output);
    break;
  case YDT_RHS_CALL:
    ydt_rhs_call_free(&element->call);
    break;
  }
}

void ydt_rule_init(YdtRule* ydt_rule) {
  ydt_lhs_init(&ydt_rule->lhs);
  list_init(&ydt_rule->rhs, sizeof(YdtRhsElement), ydt_rhs_element_free);
}

void ydt_rule_free(void* data) {
  YdtRule* rule = (YdtRule*) data;

  ydt_lhs_free(&rule->lhs);
  list_free(&rule->rhs);
}

void ydt_init(Ydt* ydt) {
  list_init(&ydt->states, sizeof(YdtState), string_free);
  list_init(&ydt->inputs, sizeof(YdtInput), ydt_input_free);
  list_init(&ydt->outputs, sizeof(YdtOutput), string_free);
  ydt->init = NULL;
  list_init(&ydt->rules, sizeof(YdtRule), ydt_rule_free);
}

void ydt_free(void* data) {
  Ydt* ydt = (Ydt*) data;

  list_free(&ydt->states);
  list_free(&ydt->inputs);
  list_free(&ydt->outputs);
  free(ydt->init);
  list_free(&ydt->rules);
}

//////////////////////////////////////////////////

void ydt_input_list_print(List* list) {
  ListNode* p = list->head;

  while(p != NULL) {
    YdtInput* input = (YdtInput*) p->data;
    printf("%s^(%d)", input->symbol, input->rank);
    if(p->next != NULL) {
      printf(", ");
    }
    p = p->next;
  }
}

void ydt_rhs_element_list_print(List* list) {
  ListNode* p = list->head;

  while(p != NULL) {
    YdtRhsElement* element = (YdtRhsElement*) p->data;
    switch(element->tag) {
    case YDT_RHS_OUTPUT:
      printf("%s ", element->output);
      break;
    case YDT_RHS_CALL:
      printf("%s(%s) ", element->call.state, element->call.variable);
      break;
    }
    p = p->next;
  }
}

void ydt_rule_list_print(List* list) {
  ListNode* p = list->head;

  while(p != NULL) {
    printf("\t\t");
    YdtRule* rule = (YdtRule*) p->data;
    printf("%s(%s(", rule->lhs.state, rule->lhs.input.symbol);
    list_print_string(&rule->lhs.variables);
    printf("))");
    printf(" → ");
    ydt_rhs_element_list_print(&rule->rhs);
    printf("\n");
    p = p->next;
  }
}

bool ydt_print(void* data) {
  Ydt* ydt = (Ydt*) data;

  printf("yDT = (Q, Σ, Δ, q_0, δ)\n");
  printf("\tQ = {");
  list_print_string(&ydt->states);
  printf("}\n");

  printf("\tΣ = {");
  ydt_input_list_print(&ydt->inputs);
  printf("}\n");

  printf("\tΔ = {");
  list_print_string(&ydt->outputs);
  printf("}\n");

  printf("\tq_0 = %s\n", ydt->init);

  printf("\tδ = {\n");
  ydt_rule_list_print(&ydt->rules);
  printf("\t}\n");

  return true;
}

void type_list_to_input(List* inputs, List* type_list) {
  ListNode* tp = type_list->head;
  while(tp != NULL) {
    Type* type = (Type*) tp->data;

    ListNode* ip = type->consts.head;
    while(ip != NULL) {
      TypeConst* type_const = (TypeConst*) ip->data;
      YdtInput input;
      input.rank = type_const->type_exprs.length;
      input.symbol = strdup(type_const->const_name);
      if(!list_contain(inputs, &input, ydt_input_cmp)) {
        list_cons(&input, inputs);
      }
      ip = ip->next;
    }

    tp = tp->next;
  }
}

void lhs_to_ydt_input(YdtInput* input, Lhs* lhs) {
  input->symbol = strdup(lhs->const_name);
  input->rank = lhs->variables.length;
}

void collect_ydt_output_from_rhs(List* outputs, List* rhs) {
  ListNode* ep = rhs->head;
  while(ep != NULL) {
    RhsElement* elm = (RhsElement*) ep->data;
    if(elm->tag == RHS_OUTPUT) {
      if(!list_contain(outputs, &elm->output, string_cmp)) {
        char* output = strdup(elm->output);
        list_cons(&output, outputs);
      }
    }
    ep = ep->next;
  }
}

void const_match_to_ydt_rule(YdtRule* rule, char* state, ConstMatch* const_match) {
  Lhs lhs = const_match->lhs;
  rule->lhs.state = strdup(state);
  rule->lhs.input.symbol = strdup(lhs.const_name);
  rule->lhs.input.rank = lhs.variables.length;
  list_copy(&rule->lhs.variables, &lhs.variables, list_copy_string);

  List rhs = const_match->rhs;
  ListNode* ep = rhs.head;
  while(ep != NULL) {
    RhsElement* elm = (RhsElement*) ep->data;
    YdtRhsElement ydt_elm;

    switch(elm->tag) {
    case RHS_OUTPUT:
      ydt_elm.tag = YDT_RHS_OUTPUT;
      ydt_elm.output = strdup(elm->output);
      break;
    case RHS_CALL:
      ydt_elm.tag = YDT_RHS_CALL;
      ydt_elm.call.state = strdup(elm->rhs_call.func_name);
      ydt_elm.call.variable = strdup(elm->rhs_call.variable);
      break;
    }

    list_cons(&ydt_elm, &rule->rhs);

    ep = ep->next;
  }

  list_reverse(&rule->rhs);
}

void func_body_to_ydt(Ydt* ydt, List* func_body_list) {
  ListNode* fp = func_body_list->head;
  while(fp != NULL) {
    FuncBody* func_body = (FuncBody*) fp->data;
    if(ydt->init == NULL) {
      ydt->init = strdup(func_body->func_name);
    }
    char *state = strdup(func_body->func_name);
    list_cons(&state, &ydt->states);

    ListNode* ep = func_body->const_matches.head;
    while(ep != NULL) {
      ConstMatch* const_match = (ConstMatch*) ep->data;

      YdtInput input;
      lhs_to_ydt_input(&input, &const_match->lhs);

      if(!list_contain(&ydt->inputs, &input, ydt_input_cmp)) {
        list_cons(&input, &ydt->inputs);
      } else {
        ydt_input_free(&input);
      }

      collect_ydt_output_from_rhs(&ydt->outputs, &const_match->rhs);

      YdtRule rule;
      ydt_rule_init(&rule);
      const_match_to_ydt_rule(&rule, state, const_match);

      list_cons(&rule, &ydt->rules);

      ep = ep->next;
    }

    fp = fp->next;
  }
}

void expr_to_ydts(List* ydts, List* type_list, List* func_list_list) {
  List inputs;
  list_init(&inputs, sizeof(YdtInput), ydt_input_free);

  type_list_to_input(&inputs, type_list);

  ListNode* flp = func_list_list->head;
  while(flp != NULL) {
    List* func_body_list = (List*) flp->data;

    Ydt ydt;
    ydt_init(&ydt);

    func_body_to_ydt(&ydt, func_body_list);

    // FIXME
    if(!list_equal_as_set(&ydt.inputs, &inputs, ydt_input_cmp)) {
      printf("input type error.\n");
      exit(1);
    }

    list_reverse(&ydt.states);
    list_reverse(&ydt.inputs);
    list_reverse(&ydt.outputs);
    list_reverse(&ydt.rules);

    list_cons(&ydt, ydts);

    flp = flp->next;
  }
}

//////////////////////////////////////////////////

char* prefix_cat(char* prefix, char* delimiter, char* str) {
  int length = strlen(prefix) + strlen(delimiter) + strlen(str) + 1;
  char* ret = (char*) malloc(sizeof(char) * length);

  strcpy(ret, prefix);
  strcat(ret, delimiter);
  strcat(ret, str);

  return ret;
}

void ydt_merge_state(Ydt* merge, Ydt* ydt) {
  char* prefix = ydt->init;
  ListNode* p = ydt->states.head;
  while(p != NULL) {
    char* state = *(char**) p->data;
    char* new_state = prefix_cat(prefix, "@", state);

    list_cons(&new_state, &merge->states);

    p = p->next;
  }
}

void ydt_merge_rule(Ydt* merge, Ydt* ydt) {
  char* prefix = ydt->init;

  ListNode* rp = ydt->rules.head;
  while(rp != NULL) {
    YdtRule* rule = (YdtRule*) rp->data;
    YdtRule new_rule;
    ydt_rule_init(&new_rule);

    new_rule.lhs.state = prefix_cat(prefix, "@", rule->lhs.state);
    ydt_input_set(&new_rule.lhs.input, &rule->lhs.input);
    list_copy(&new_rule.lhs.variables, &rule->lhs.variables, list_copy_string);

    ListNode* ep = rule->rhs.head;
    while(ep != NULL) {
      YdtRhsElement* elm = (YdtRhsElement*) ep->data;
      YdtRhsElement new_elm;
      new_elm.tag = elm->tag;
      switch(elm->tag) {
      case YDT_RHS_OUTPUT:
        new_elm.output = strdup(elm->output);
        break;
      case YDT_RHS_CALL:
        new_elm.call.state = prefix_cat(prefix, "@", elm->call.state);
        new_elm.call.variable = strdup(elm->call.variable);
        break;
      }
      list_cons(&new_elm, &new_rule.rhs);
      ep = ep->next;
    }
    list_reverse(&new_rule.rhs);
    list_cons(&new_rule, &merge->rules);

    rp = rp->next;
  }
}

void ydt_merge(Ydt* merge, Ydt* ydt1, Ydt* ydt2) {
  ydt_merge_state(merge, ydt1);
  ydt_merge_state(merge, ydt2);

  if(!list_equal_as_set(&ydt1->inputs, &ydt2->inputs, ydt_input_cmp)) {
    printf("there is some different input symbols.\n");
    exit(1);
  }

  list_copy(&merge->inputs, &ydt1->inputs, list_copy_ydt_input);

  if(!list_equal_as_set(&ydt1->outputs, &ydt2->outputs, string_cmp)) {
    printf("there is some different output symbols.\n");
    exit(1);
  }

  list_copy(&merge->outputs, &ydt1->outputs, list_copy_string);

  ydt_merge_rule(merge, ydt1);
  ydt_merge_rule(merge, ydt2);

  list_reverse(&merge->states);
}
