#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "poly.h"
#include "groebner.h"

#include "ideal.h"

void ideal_init(Ideal* ideal, int n) {
  ideal->n = n;
  list_init(&ideal->base, sizeof(Poly), poly_free);
}

void ideal_free(void* data) {
  Ideal* ideal = (Ideal*) data;

  list_free(&ideal->base);
}

void ideal_print(void* data) {
  Ideal* ideal = (Ideal*) data;

  printf("<");
  ListNode* p = ideal->base.head;
  while(p != NULL) {
    Poly* poly = (Poly*) p->data;
    poly_print(poly);

    if(p->next != NULL) {
      printf(", ");
    }
    p = p->next;
  }
  printf(">");
}

void ideal_one(Ideal* ideal) {
  Poly one;
  poly_init(&one, ideal->n);
  poly_add_ui(&one, 1);

  list_cons(&one, &ideal->base);
}

void ideal_mul_t(Ideal* tI, Ideal* I, Term* t) {
  ListNode* p = I->base.head;
  while(p != NULL) {
    Poly* base = (Poly*) p->data;
    Poly t_poly;
    poly_init(&t_poly, base->n);
    poly_add_p(&t_poly, base);
    poly_mul_t(&t_poly, t);
    list_cons(&t_poly, &tI->base);
    p = p->next;
  }
}

void ideal_mul_p(Ideal* pI, Ideal* I, Poly* poly) {
  ListNode* p = I->base.head;
  while(p != NULL) {
    Poly* base = (Poly*) p->data;
    Poly p_poly;
    poly_init(&p_poly, base->n);
    poly_add_p(&p_poly, base);
    poly_mul_p(&p_poly, poly);
    list_cons(&p_poly, &pI->base);
    p = p->next;
  }
}

void ideal_union(Ideal* I_union_J, Ideal* I, Ideal* J) {
  List left;
  list_init(&left, sizeof(Poly), poly_free);
  list_copy(&left, &I->base, list_copy_poly);

  List right;
  list_init(&right, sizeof(Poly), poly_free);
  list_copy(&right, &J->base, list_copy_poly);

  list_append(&I_union_J->base, &left);
  list_append(&I_union_J->base, &right);
}

void ideal_product(Ideal* I_prod_J, Ideal* I, Ideal* J) {
  if(I->base.length == 0 && J->base.length == 0) {
    printf("<> ∩ <> is illegal.\n");
    exit(1);
  }

  Poly one;
  poly_init(&one, I_prod_J->n);
  poly_add_ui(&one, 1);
  if(I->base.length == 0) {
    I_prod_J->n = J->n;
    list_cons(&one, &I->base);
  } else if(J->base.length == 0) {
    I_prod_J->n = I->n;
    list_cons(&one, &J->base);
  }

  ListNode* ipp = I->base.head;
  while(ipp != NULL) {
    Poly* lp = (Poly*) ipp->data;

    ListNode* jpp = J->base.head;
    while(jpp != NULL) {
      Poly* jp = (Poly*) jpp->data;

      Poly ij;
      poly_init(&ij, I_prod_J->n);
      poly_add_p(&ij, lp);
      poly_mul_p(&ij, jp);

      list_cons(&ij, &I_prod_J->base);

      jpp = jpp->next;
    }
    ipp = ipp->next;
  }
}

void ideal_intersect(Ideal* I_inter_J, Ideal* I, Ideal* J) {
  Ideal tI;
  ideal_init(&tI, I->n);
  Term t;
  term_init(&t, I->n);
  term_set_ui(&t, 1);
  t.mi[0] = 1;
  ideal_mul_t(&tI, I, &t);

  Ideal t_1J;
  ideal_init(&t_1J, J->n);
  Poly t_1;
  poly_init(&t_1, J->n);
  poly_add_t(&t_1, &t);
  poly_sub_ui(&t_1, 1);
  ideal_mul_p(&t_1J, J, &t_1);

  Ideal tIp1_tJ;
  ideal_init(&tIp1_tJ, I_inter_J->n);
  ideal_union(&tIp1_tJ, &tI, &t_1J);

  ideal_groebner(I_inter_J, &tIp1_tJ, 1);

  ideal_eliminate_k(I_inter_J, 1);

  term_free(&t);
  poly_free(&t_1);
  ideal_free(&tIp1_tJ);
}

void ideal_eliminate_k(Ideal* ideal, int k) {
  ListNode* current = ideal->base.head;
  ListNode* prev = NULL;
  ListNode* next = NULL;

  while(current != NULL) {
    Poly* poly = (Poly*) current->data;
    bool eliminate = false;

    ListNode* tp = poly->terms.head;
    while(tp != NULL) {
      Term* term = (Term*) tp->data;

      for(int i = 0; i < k; i++) {
        if(term->mi[i] != 0) {
          eliminate = true;
          break;
        }
      }

      if(eliminate) {
        break;
      }
      tp = tp->next;
    }

    if(eliminate) {
      next = current->next;
      ideal->base.free_func(poly);
      free(current);
      ideal->base.length--;
      current = next;
      if(prev != NULL) {
        prev->next = next;
      } else {
        ideal->base.head = NULL;
      }
    } else {
      prev = current;
      current = current->next;
    }
  }
}

void ideal_divide(List* quorems, Poly* dividend, Ideal* divisors) {
  Poly* median_dividend = dividend;
  ListNode* pp = divisors->base.head;
  while(pp != NULL) {
    Poly* divisor = (Poly*) pp->data;
    Quorem quorem;
    quorem_init(&quorem, divisor->n);

    poly_divide(&quorem, median_dividend, divisor);

    list_cons(&quorem, quorems);
    median_dividend = &((Quorem*)quorems->head->data)->remainder;

    pp = pp->next;
  }
}
