#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "poly.h"
#include "ydt.h"
#include "util.h"

#include "ydt_to_poly.h"

int z_to_x(int max_rank, int z_base, int index_z, int i) {
  int j = (index_z - z_base) / 2;
  int k = (index_z - z_base) % 2;
  return x_ijk(max_rank, i, j, k);
}

void poly_rule_init(PolyRule* poly_rule, int states_size, int max_rank) {
  int dim = dimension(states_size, max_rank);
  poly_rule->states_size = states_size;
  poly_rule->max_rank = max_rank;
  poly_init(&poly_rule->poly[0], dim);
  poly_init(&poly_rule->poly[1], dim);
}

void poly_rule_free(void *data) {
  PolyRule* poly_rule = (PolyRule*) data;
  free(poly_rule->state);
  ydt_input_free(&poly_rule->input);
  poly_free(&poly_rule->poly[0]);
  poly_free(&poly_rule->poly[1]);
}

void eliminate_y(PolyRule* poly_rule, Poly* poly) {
  ListNode* p = poly->terms.head;

  while(p != NULL) {
    Term* term = (Term*) p->data;
    if(term->mi[0] == 0) {
      poly_add_t(&poly_rule->poly[0], term);
    } else {
      term->mi[0] = 0;
      poly_add_t(&poly_rule->poly[1], term);
    }
    p = p->next;
  }
}

void ydt_rule_to_poly_rule(PolyRule* poly_rule, Ydt* ydt, YdtRule* rule) {
  poly_rule->state = strdup(rule->lhs.state);
  ydt_input_set(&poly_rule->input, &rule->lhs.input);

  int states_size = poly_rule->states_size;
  int max_rank = poly_rule->max_rank;
  int dim = dimension(states_size, max_rank);

  List states = ydt->states;
  List outputs = ydt->outputs;
  List variables = rule->lhs.variables;

  // 規則の右辺を右から辿ることで，計算を楽にする
  List rev_rhs;
  list_init(&rev_rhs, sizeof(YdtRhsElement), ydt_rhs_element_free);
  list_copy(&rev_rhs, &rule->rhs, list_copy_ydt_rhs_element);
  list_reverse(&rev_rhs);

  int s = outputs.length + 1;

  Poly poly;
  poly_init(&poly, dim);
  Term y;
  term_init(&y, dim);
  term_set_ui(&y, 1);
  y.mi[0] = 1;
  poly_add_t(&poly, &y);
  term_free(&y);

  Term x;
  term_init(&x, dim);
  int i, j, ij0, ij1;

  ListNode *p = rev_rhs.head;
  while(p != NULL) {
    YdtRhsElement* elm = (YdtRhsElement*) p->data;
    switch(elm->tag) {
    case YDT_RHS_OUTPUT:
      if(strcmp(elm->output, "\"\"") == 0) {
        poly_mul_ui(&poly, 0);
      } else {
        poly_mul_ui(&poly, s);
        poly_add_ui(&poly, list_index_at(&outputs, &elm->output, string_cmp)+1);
      }
      break;
    case YDT_RHS_CALL:
      i = list_index_at(&variables, &elm->call.variable, string_cmp);
      j = list_index_at(&states, &elm->call.state, string_cmp);
      ij0 = x_ijk(max_rank, i, j, 0);
      ij1 = ij0 + 1;
      term_set_ui(&x, 1);

      x.mi[ij1] = 1;
      poly_mul_t(&poly, &x);
      x.mi[ij1] = 0;

      x.mi[ij0] = 1;
      poly_add_t(&poly, &x);
      x.mi[ij0] = 0;
      break;
    }
    p = p->next;
  }
  term_free(&x);

  if(rev_rhs.length == 0) {
    poly_mul_ui(&poly, 0);
  }

  eliminate_y(poly_rule, &poly);

  list_free(&rev_rhs);
}

void ydt_poly_init(YdtPoly* ydt_poly) {
  list_init(&ydt_poly->states, sizeof(YdtState), string_free);
  list_init(&ydt_poly->inputs, sizeof(YdtInput), ydt_input_free);
  list_init(&ydt_poly->rules, sizeof(PolyRule), poly_rule_free);
}

void ydt_poly_free(void* data) {
  YdtPoly* ydt_poly = (YdtPoly*) data;

  list_free(&ydt_poly->states);
  list_free(&ydt_poly->inputs);
  list_free(&ydt_poly->rules);
}

void ydt_to_ydt_poly(YdtPoly* ydt_poly, Ydt* ydt) {
  list_copy(&ydt_poly->states, &ydt->states, list_copy_string);
  list_copy(&ydt_poly->inputs, &ydt->inputs, list_copy_ydt_input);

  int states_size = ydt->states.length;
  int max_rank = ((YdtInput*) list_max(&ydt->inputs, ydt_input_cmp))->rank;

  ydt_poly->states_size = states_size;
  ydt_poly->max_rank = max_rank;

  ListNode* rp = ydt->rules.head;
  while(rp != NULL) {
    YdtRule* rule = (YdtRule*) rp->data;
    PolyRule poly_rule;
    poly_rule_init(&poly_rule, states_size, max_rank);

    ydt_rule_to_poly_rule(&poly_rule, ydt, rule);

    list_cons(&poly_rule, &ydt_poly->rules);

    rp = rp->next;
  }
}

void ydt_poly_rule_list_print(List* rules) {
  ListNode* p = rules->head;

  while(p != NULL) {
    printf("\t\t");
    PolyRule* rule = (PolyRule*) p->data;
    printf("%s(%s) → ", rule->state, rule->input.symbol);
    poly_print(&rule->poly[0]);
    printf(" + (");
    poly_print(&rule->poly[1]);
    printf(") y\n");

    p = p->next;
  }
}

void ydt_poly_print(void* data) {
  YdtPoly* ydt_poly = (YdtPoly*) data;

  printf("yDT_p = (Q, Σ, δ)\n");
  printf("\tQ = {");
  list_print_string(&ydt_poly->states);
  printf("}\n");

  printf("\tΣ = {");
  ydt_input_list_print(&ydt_poly->inputs);
  printf("}\n");

  printf("\tδ = {\n");
  ydt_poly_rule_list_print(&ydt_poly->rules);
  printf("\t}\n");
}
