#ifndef _YDT_H_
#define _YDT_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "list.h"

typedef char* YdtState;

typedef struct {
  int rank;
  char* symbol;
} YdtInput;

void ydt_input_set(YdtInput* dst, YdtInput* src);
void ydt_input_free(void* data);
int ydt_input_cmp(void *data1, void* data2);
void* list_copy_ydt_input(void* data);

void ydt_input_list_print(List* list);

typedef char* YdtOutput;

typedef char* Variable;

typedef struct {
  YdtState state;
  YdtInput input;
  List     variables;
} YdtLhs;

void ydt_lhs_init();
void ydt_lhs_free(void *data);

enum YdtRhsTag {
  YDT_RHS_OUTPUT, YDT_RHS_CALL
};

typedef struct {
  YdtState state;
  Variable variable;
} YdtRhsCall;

void free_ydt_rhs_call(void *data);

typedef struct {
  enum YdtRhsTag tag;
  union {
    YdtOutput output;
    YdtRhsCall call;
  };
} YdtRhsElement;

void ydt_rhs_element_free(void* data);
void* list_copy_ydt_rhs_element(void* data);

typedef struct {
  YdtLhs lhs;
  List   rhs;
} YdtRule;

void ydt_rule_init(YdtRule* ydt_rule);
void ydt_rule_free(void* data);

typedef struct {
  List     states;
  List     inputs;
  List     outputs;
  YdtState init;
  List     rules;
} Ydt;

void ydt_init(Ydt* ydt);
void ydt_free(void* data);

bool ydt_print(void* data);

void expr_to_ydts(List* ydts, List* type_list, List* func_list);

void ydt_merge(Ydt* merge, Ydt* ydt1, Ydt* ydt2);

#endif
