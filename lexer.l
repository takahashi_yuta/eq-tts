%{
%}
white       [ \t\n]
integer     [0-9]+
ident       [a-zA-Z][a-zA-Z0-9_]*
string      "\""([^\n\"\\]*(\\[.\n])*)*"\""
%%
"type"        { return L_TYPE;      }
"of"          { return L_OF;        }
"let"         { return L_LET;       }
"rec"         { return L_REC;       }
"function"    { return L_FUNCTION;  }
"and"         { return L_AND;       }
"->"          { return L_ARROW;     }
{ident}       { yylval.string = yytext; return L_IDENT; }
{string}      { yylval.string = yytext; return L_STRING; }
[*=()|^,]     { return yytext[0]; }
{white}+
%%
