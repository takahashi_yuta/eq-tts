#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "list.h"
#include "util.h"

#include "expr.h"

void lhs_free(void *data) {
  Lhs* lhs = (Lhs*) data;
  free(lhs->const_name);
  list_free(&lhs->variables);
}

void rhs_element_free(void* data) {
  RhsElement* element = (RhsElement*) data;
  switch(element->tag) {
  case RHS_OUTPUT:
    free(element->output);
    break;
  case RHS_CALL:
    free(element->rhs_call.func_name);
    free(element->rhs_call.variable);
    break;
  }
}

void const_match_free(void *data) {
  ConstMatch *const_match = (ConstMatch*) data;

  lhs_free(&const_match->lhs);
  list_free(&const_match->rhs);
}

void func_body_free(void* data) {
  FuncBody* func_body = (FuncBody*) data;

  free(func_body->func_name);
  list_free(&func_body->const_matches);
}

void func_body_list_free(void* data) {
  List* list = (List*) data;

  list_free(list);
}

void type_const_free(void* data) {
  TypeConst* type_const = (TypeConst*) data;

  free(type_const->const_name);
  list_free(&type_const->type_exprs);
}

void type_free(void* data) {
  Type* type = (Type*) data;

  free(type->type_name);
  list_free(&type->consts);
}
