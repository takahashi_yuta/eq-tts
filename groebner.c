#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "poly.h"
#include "ideal.h"

#include "groebner.h"

#define DEBUG_FGB

#define LIBMODE 2

#define USE_MY_OWN_IO 0

#if USE_MY_OWN_IO
#define CALL_FGB_DO_NOT_DEFINE
#endif /*  USE_MY_OWN_IO */

#include <gmp.h>
#include "call_fgb.h"
#include "protocol_maple.h"

#define FGb_MAXI_BASE 100000

extern double ideal_time;
extern double convert_time;
extern double setup_time;

void ideal_groebner(Ideal* groebner, Ideal* ideal, int k) {
  Dpol input_basis[FGb_MAXI_BASE];
  int  global_nb = 0;
  Dpol output_basis[FGb_MAXI_BASE];
  int  n_input = ideal->base.length;
  int  nb_vars = ideal->n;

  struct rusage usage;
  struct timeval ut1, ut2;
  struct timeval st1, st2;
  struct timeval s, e;

  char** vars;
  dummy_variables(&vars, nb_vars);

  gettimeofday(&s, NULL);
  getrusage(RUSAGE_SELF, &usage);
  ut1 = usage.ru_utime;
  st1 = usage.ru_stime;

  FGB(saveptr)();
  threads_FGb(4);
  init_FGb_Integers();

  FGB(PowerSet)(k, nb_vars - k, vars);

  getrusage(RUSAGE_SELF, &usage);
  ut2 = usage.ru_utime;
  st2 = usage.ru_stime;
  gettimeofday(&e, NULL);

  ListNode* pp = ideal->base.head;
  while(pp != NULL) {
    Poly* poly = (Poly*) pp->data;
    int size = poly->terms.length;
    Dpol_INT prev = FGB(creat_poly)(size);

    input_basis[global_nb++] = prev;

    int index_term = 0;

    ListNode* tp = poly->terms.head;
    while(tp != NULL) {
      Term* term = (Term*) tp->data;
      FGB(set_expos2)(prev, index_term, term->mi, nb_vars);
      FGB(set_coeff_gmp)(prev, index_term, term->coef);
      index_term++;
      tp = tp->next;
    }
    FGB(full_sort_poly2)(prev);

    pp = pp->next;
  }

  setup_time += (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6;

  SFGB_Options options;

  FGb_set_default_options(&options);
  options._env._force_elim = 0;
  options._env._index = 10000000;
#ifdef DEBUG_FGB
  options._verb = 1; /* display useful infos during the computation */
#else
  options._verb = 0;
#endif

  printf("========== start calcurate ideal ==========\n");  

  gettimeofday(&s, NULL);
  getrusage(RUSAGE_SELF, &usage);
  ut1 = usage.ru_utime;
  st1 = usage.ru_stime;  

  double t0;
  int nb = FGB(fgb)(input_basis, n_input, output_basis, FGb_MAXI_BASE, &t0, &options);
 
  getrusage(RUSAGE_SELF, &usage);
  ut2 = usage.ru_utime;
  st2 = usage.ru_stime;
  gettimeofday(&e, NULL);

  ideal_time += (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6;
  printf("ideal: %lf\n", ideal_time);

  printf("========== end calcurate ideal ==========\n");  

  gettimeofday(&s, NULL);
  getrusage(RUSAGE_SELF, &usage);
  ut1 = usage.ru_utime;
  st1 = usage.ru_stime;

  for(int i = 0; i < nb; i++) {
    int nb_terms = FGB(nb_terms)(output_basis[i]);
    unsigned* mons = (unsigned*) malloc(sizeof(unsigned) * nb_vars*nb_terms);
    mpz_ptr* cfs = (mpz_ptr*) malloc(nb_terms * sizeof(mpz_ptr));
    int code = FGB(export_poly_INT_gmp2)(nb_vars, nb_terms, cfs, mons, output_basis[i]);

    Poly poly;
    poly_init(&poly, nb_vars);
    for(int j = 0; j < nb_terms; j++) {
      unsigned* ei = mons + j * nb_vars;
      Term term;
      term_init(&term, nb_vars);
      term_set_mpz(&term, cfs[j]);
      for(int k = 0; k < nb_vars; k++) {
        term.mi[k] = ei[k];
      }
      poly_add_t(&poly, &term);
      term_free(&term);
    }

    list_reverse(&poly.terms);

    list_cons(&poly, &groebner->base);
  }

  dummy_variables_free(&vars, nb_vars);

  FGB(restoreptr)();
  FGB(reset_memory)();

  getrusage(RUSAGE_SELF, &usage);
  ut2 = usage.ru_utime;
  st2 = usage.ru_stime;
  gettimeofday(&e, NULL);

  convert_time += (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6;
}

// 本当はグレブナ基底もソートした方がよい
bool ideal_contain(Ideal* groebner, Poly* poly) {
  if(poly_is_zero(poly)) {
    return true;
  }

  bool retval = false;
  Poly median_dividend;
  poly_init(&median_dividend, poly->n);
  poly_add_p(&median_dividend, poly);
  bool divisionoccured;

  do {
    List quorems;
    list_init(&quorems, sizeof(Quorem), quorem_free);

    divisionoccured = false;
    ideal_divide(&quorems, &median_dividend, groebner);

    ListNode* p = quorems.head;
    while(p != NULL) {
      Quorem* quorem = (Quorem*) p->data;
      if(!poly_is_zero(&quorem->quotient)) {
        divisionoccured = true;

        poly_free(&median_dividend);
        poly_init(&median_dividend, quorem->remainder.n);
        poly_add_p(&median_dividend, &quorem->remainder);
        list_reverse(&median_dividend.terms);

        if(poly_is_zero(&quorem->remainder)) {
          retval = true;
          break;
        }
      }
      p = p->next;
    }
    list_free(&quorems);
  } while(divisionoccured);

  return retval;
}

void ideal_groebner_d(Ideal* groebner_d, Ideal* ideal, int k, int d) {
  Ideal groebner;
  ideal_init(&groebner, ideal->n);

  ideal_groebner(&groebner, ideal, k);

  ListNode* p = groebner.base.head;
  while(p != NULL) {
    Poly* poly = (Poly*) p->data;
    int degree = poly_total_degree(poly);
    if(degree <= d) {
      Poly poly_d;
      poly_init(&poly_d, poly->n);
      poly_add_p(&poly_d, poly);
      list_reverse(&poly_d.terms);
      list_cons(&poly_d, &groebner_d->base);
    }
    p = p->next;
  }

  ideal_free(&groebner);
}
