#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <gmp.h>

#include "list.h"
#include "util.h"
#include "poly.h"

////////////////////////////////////////////////////////////
// Term
////////////////////////////////////////////////////////////

void term_init(Term* term, int n) {
  term->n  = n;
  term->mi = (int*) calloc(n, sizeof(int));
  mpz_init(term->coef);
}

void term_set(Term* dst, Term* src) {
  if(dst->n != src->n) {
    return;
  }

  mpz_set(dst->coef, src->coef);
  memcpy(dst->mi, src->mi, sizeof(int) * src->n);
}

void term_free(void *data) {
  Term* term = (Term*) data;

  free(term->mi);
  mpz_clear(term->coef);
}

int term_lex_cmp(void *data1, void* data2) {
  Term* term1 = (Term*) data1;
  Term* term2 = (Term*) data2;

  for(int i = 0; i < term1->n; i++) {
    if(term1->mi[i] > term2->mi[i]) {
      return 1;
    } else if(term1->mi[i] < term2->mi[i]) {
      return -1;
    }
  }

  return 0;
}

void* list_copy_term(void* data) {
  Term* term = (Term*) data;
  Term* ret = (Term*) malloc(sizeof(Term));
  term_init(ret, term->n);
  term_set(ret, term);

  return ret;
}

void term_set_mpz(Term* term, mpz_t coef) {
  mpz_set(term->coef, coef);
}

void term_set_str(Term* term, char* coef) {
  mpz_set_str(term->coef, coef, 10);
}

void term_set_ui(Term *term, unsigned long int coef) {
  mpz_set_ui(term->coef, coef);
}

void term_mul_mpz(Term* l, mpz_t z) {
  mpz_mul(l->coef, l->coef, z);
}

void term_mul_t(Term* l, Term* r) {
  if(l->n != r->n) {
    return;
  }

  mpz_mul(l->coef, l->coef, r->coef);

  for(int i = 0; i < l->n; i++) {
    l->mi[i] = l->mi[i] + r->mi[i];
  }
}

bool term_same_mi(Term* l, Term* r) {
  if(l->n != r->n) {
    return false;
  }

  for(int i = 0; i < l->n; i++) {
    if(l->mi[i] != r->mi[i]) {
      return false;
    }
  }

  return true;
}

void term_neg(Term* term) {
  mpz_neg(term->coef, term->coef);
}

bool term_can_divide(Term* dividend, Term* divisor) {
  if(mpz_sgn(divisor->coef) == 0) {
    return false;
  }

  for(int i = 0; i < dividend->n; i++) {
    if(dividend->mi[i] < divisor->mi[i]) {
      return false;
    }
  }
  return true;
}

void term_divide(Term* quotient, Term* dividend, Term* divisor) {
  mpz_div(quotient->coef, dividend->coef, divisor->coef);

  for(int i = 0; i < quotient->n; i++) {
    quotient->mi[i] = dividend->mi[i] - divisor->mi[i];
  }
}

int term_degree(Term* term) {
  int degree = 0;

  for(int i = 0; i < term->n; i++) {
    degree += term->mi[i];
  }

  return degree;
}

void term_print(Term* term) {
  mpz_out_str(stdout, 10, term->coef);

  for(int i = 0; i < term->n; i++) {
    if(term->mi[i] != 0) {
      printf(" x_%d", i);
      if(term->mi[i] > 1) {
        printf("^%d", term->mi[i]);
      }
    }
  }
}

////////////////////////////////////////////////////////////
// Poly
////////////////////////////////////////////////////////////

void poly_init(Poly* poly, int n) {
  poly->n = n;
  list_init(&poly->terms, sizeof(Term), term_free);
}

void poly_free(void* data) {
  Poly* poly = (Poly*) data;
  list_free(&poly->terms);
}

void poly_set(Poly* dst, Poly* src) {
  list_copy(&dst->terms, &src->terms, list_copy_term);
}

void poly_neg(Poly *poly) {
  ListNode* p = poly->terms.head;
  while(p != NULL) {
    Term* term = (Term*) p->data;
    term_neg(term);
    p = p->next;
  }
}

void poly_add_z(Poly* poly, mpz_t z) {
  Term term;
  term_init(&term, poly->n);
  term_set_mpz(&term, z);

  poly_add_t(poly, &term);

  term_free(&term);
}

void poly_add_ui(Poly* poly, unsigned long int z) {
  Term term;
  term_init(&term, poly->n);
  term_set_ui(&term, z);

  poly_add_t(poly, &term);

  term_free(&term);
}

void poly_add_t(Poly* poly, Term* r) {
  if(mpz_sgn(r->coef) == 0) {
    return;
  }

  ListNode* p = poly->terms.head;
  while(p != NULL) {
    Term* l = (Term*) p->data;
    if(term_same_mi(l, r)) {
      mpz_add(l->coef, l->coef, r->coef);
      if(mpz_sgn(l->coef) == 0) {
        list_remove(&poly->terms, l);
      }
      return;
    }
    p = p->next;
  }
  Term copy;
  term_init(&copy, r->n);
  term_set(&copy, r);
  list_cons(&copy, &poly->terms);
}

void poly_add_p(Poly* l, Poly* r) {
  if(l->n != r->n) {
    return;
  }

  ListNode* p = r->terms.head;
  while(p != NULL) {
    Term* term = (Term*) p->data;
    poly_add_t(l, term);
    p = p->next;
  }
}

void poly_sub_ui(Poly* poly, unsigned long int z) {
  Term term;
  term_init(&term, poly->n);
  term_set_ui(&term, z);
  term_neg(&term);

  poly_add_t(poly, &term);

  term_free(&term);
}

void poly_sub_t(Poly* poly, Term* t) {
  Term term;
  term_init(&term, poly->n);
  term_set(&term, t);
  term_neg(&term);

  poly_add_t(poly, &term);

  term_free(&term);
}

void poly_sub_p(Poly* l, Poly* r) {
  if(l->n != r->n) {
    return;
  }

  ListNode* p = r->terms.head;
  while(p != NULL) {
    Term* term = (Term*) p->data;
    Term neg;
    term_init(&neg, term->n);
    term_set(&neg, term);
    term_neg(&neg);
    poly_add_t(l, &neg);
    term_free(&neg);
    p = p->next;
  }
}

void poly_mul_mpz(Poly* poly, mpz_t z) {
  ListNode* p = poly->terms.head;

  while(p != NULL) {
    Term* term = (Term*) p->data;
    mpz_mul(term->coef, term->coef, z);
    p = p->next;
  }
}

void poly_mul_ui(Poly *dst, unsigned long int z) {
  ListNode *p = dst->terms.head;

  while(p != NULL) {
    Term* term = (Term*) p->data;
    mpz_mul_ui(term->coef, term->coef, z);
    p = p->next;
  }
}

void poly_mul_t(Poly* poly, Term* term) {
  ListNode* p = poly->terms.head;

  while(p != NULL) {
    Term* t = (Term*) p->data;
    term_mul_t(t, term);

    p = p->next;
  }
}

void poly_mul_p(Poly* l, Poly* r) {
  if(l->n != r->n) {
    return;
  }

  Poly copy;
  poly_init(&copy, l->n);
  poly_set(&copy, l);

  poly_free(l);
  poly_init(l, copy.n);

  ListNode* lp = copy.terms.head;
  while(lp != NULL) {
    Term* lterm = (Term*) lp->data;

    ListNode* rp = r->terms.head;
    while(rp != NULL) {
      Term* rterm = (Term*) rp->data;
      Term term;
      term_init(&term, rterm->n);
      term_set(&term, lterm);
      term_mul_t(&term, rterm);

      poly_add_t(l, &term);

      term_free(&term);

      rp = rp->next;
    }
    lp = lp->next;
  }

  poly_free(&copy);
}

void poly_print(Poly* poly) {
  ListNode* p = poly->terms.head;

  if(poly->terms.length == 0) {
    printf("0");
    return;
  }

  while(p != NULL) {
    Term* term = (Term*) p->data;
    term_print(term);

    if(p->next != NULL) {
      printf(" + ");
    }

    p = p->next;
  }
}

void* list_copy_poly(void* data) {
  Poly* poly = (Poly*) data;
  Poly* ret = (Poly*) malloc(sizeof(Poly));
  poly_init(ret, poly->n);
  poly_add_p(ret, poly);

  return ret;
}

void dummy_variables(char ***vars_p, int size) {
  *vars_p = (char**) malloc(sizeof(char*) * size);

  for(int i = 0; i < size; i++) {
    char *var = (char*) malloc(sizeof(char) * (2 + digit(size) + 1));
    sprintf(var, "x%d", i);
    (*vars_p)[i] = var;
  }
}

void dummy_variables_free(char ***vars_p, int size) {
  char** vars = *vars_p;

  for(int i = 0; i < size; i++) {
    free(vars[i]);
  }

  free(vars);
}

void poly_lt(Term* lt, Poly* poly) {
  if(poly->terms.length == 0) {
    term_set(lt, 0);
    return;
  }

  term_set(lt, (Term*) poly->terms.head->data);
}

bool poly_is_zero(Poly* poly) {
  // 多項式に関する操作が正しく行われいてるならば，poly->terms.lenght == 0 を調べるだけでいい．
  // 正しい操作で閉じている気がしないため，項のリストに対して調べるように実装．
  ListNode* p = poly->terms.head;
  while(p != NULL) {
    Term* term = (Term*) p->data;
    if(mpz_sgn(term->coef) != 0) {
      return false;
    }

    p = p->next;
  }

  return true;
}

void quorem_init(Quorem* quorem, int n) {
  mpz_init(quorem->normalizer);
  mpz_set_ui(quorem->normalizer, 1);
  poly_init(&quorem->quotient, n);
  poly_init(&quorem->remainder, n);
}

void quorem_free(void* data) {
  Quorem* quorem = (Quorem*) data;
  mpz_clear(quorem->normalizer);
  poly_free(&quorem->quotient);
  poly_free(&quorem->remainder);
}

bool poly_divide(Quorem* quorem,
                 Poly* dividend,
                 Poly* divisor) {
  if(poly_is_zero(dividend)) {
    return false;
  }

  bool divisionoccured = false;

  // list_sort_desc(&dividend->terms, term_lex_cmp);
  // list_sort_desc(&divisor->terms, term_lex_cmp);

  Poly median_dividend;
  poly_init(&median_dividend, dividend->n);
  poly_add_p(&median_dividend, dividend);

  Term lt_dividend;
  term_init(&lt_dividend, dividend->n);
  poly_lt(&lt_dividend, dividend);
  Term lt_divisor;
  term_init(&lt_divisor, divisor->n);
  poly_lt(&lt_divisor, divisor);

  if(term_can_divide(&lt_dividend, &lt_divisor)) {
    mpz_t lcm;
    mpz_init(lcm);

    mpz_lcm(lcm, lt_dividend.coef, lt_divisor.coef);

    if(mpz_cmp(lt_dividend.coef, lt_divisor.coef) != 0 &&
       mpz_cmp(lcm, lt_divisor.coef) >= 0) {
      mpz_mul(quorem->normalizer, quorem->normalizer, lcm);
      term_mul_mpz(&lt_dividend, lcm);
      poly_mul_mpz(&quorem->quotient, lcm);
      poly_mul_mpz(&median_dividend, lcm);
    }

    mpz_clear(lcm);

    Term a;
    term_init(&a, quorem->quotient.n);
    term_divide(&a, &lt_dividend, &lt_divisor);

    Poly sub;
    poly_init(&sub, a.n);
    poly_add_p(&sub, divisor);
    poly_mul_t(&sub, &a);

    poly_sub_p(&median_dividend, &sub);

    poly_add_t(&quorem->quotient, &a);

    divisionoccured = true;

    poly_free(&sub);
    term_free(&a);
  } else {
    poly_add_p(&quorem->remainder, &median_dividend);
  }

  term_free(&lt_dividend);
  term_free(&lt_divisor);

  if(divisionoccured) {
    poly_divide(quorem, &median_dividend, divisor);
    list_sort_desc(&quorem->quotient.terms, term_lex_cmp);
    list_sort_desc(&quorem->remainder.terms, term_lex_cmp);
  }

  poly_free(&median_dividend);

  return divisionoccured;
}

bool poly_can_divide(Poly* dividend, Poly* divisor) {
  Term lt_dividend;
  term_init(&lt_dividend, dividend->n);
  poly_lt(&lt_dividend, dividend);
  Term lt_divisor;
  term_init(&lt_divisor, divisor->n);
  poly_lt(&lt_divisor, divisor);

  bool retval = term_can_divide(&lt_dividend, &lt_divisor);

  term_free(&lt_dividend);
  term_free(&lt_divisor);

  return retval;
}

int poly_total_degree(Poly* poly) {
  int total_degree = 0;
  ListNode* p = poly->terms.head;
  while(p != NULL) {
    Term* term = (Term*) p->data;
    int degree = term_degree(term);

    if(total_degree < degree) {
      total_degree = degree;
    }
    p = p->next;
  }

  return total_degree;
}

int poly_cmp(void* data1, void* data2) {
  Poly* left = (Poly*) data1;
  Poly* right = (Poly*) data2;

  list_sort_desc(&left->terms, term_lex_cmp);
  list_sort_desc(&right->terms, term_lex_cmp);

  ListNode* ltp = left->terms.head;
  ListNode* rtp = right->terms.head;
  while(ltp != NULL && rtp != NULL) {
    Term* lt = (Term*) ltp->data;
    Term* rt = (Term*) rtp->data;

    for(int i = 0; i < lt->n; i++) {
      if(lt->mi[i] > rt->mi[i]) {
        return 1;
      } else if(lt->mi[i] < rt->mi[i]) {
        return -1;
      }

      int cmp_coef = mpz_cmp(lt->coef, rt->coef);
      if(cmp_coef > 0) {
        return 1;
      } else if(cmp_coef < 0) {
        return -1;
      }
    }

    ltp = ltp->next;
    rtp = rtp->next;
  }

  return 0;
}
