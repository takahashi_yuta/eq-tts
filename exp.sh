#!/bin/sh

rm $5/output_$1-$4
rm $5/TOP_$1-$4

./a.out $1 $2 $3 $4 < "$5/sample.eq" 2>&1 | tee "$5/output_$1-$4" &

pid=$(pidof a.out)

echo "pid = $pid"

trap 'kill $pid; exit 1' 2

file="$5/TOP_$1-$4"

while true
do
	# date=$(date '+%Y%m%d %H:%M:%S')
	ps ho pid p $pid > /dev/null
	if [ $? = 0 ]; then
  		top=$(echo -n $(top -b -c -n 1 -d 0 -p $pid | awk -v pid="$pid" '$1 == pid'))
		echo ""
		echo $top
		mem=$(echo $top | cut -d " " -f 5,6,11)
      		echo "${mem}" >> $file 
		sleep 1
	else
		exit 1
	fi
done
