#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <gmp.h>

extern char* yytext;

#include "parser.tab.c"
#include "lex.yy.c"

#include "list.h"
#include "util.h"
#include "poly.h"
#include "ydt.h"
#include "ydt_to_poly.h"
#include "ideal.h"
#include "poly.h"
#include "groebner.h"
#include "algorithm.h"

#include "test.h"

List type_list;
List func_list_list;

double ideal_time;
double convert_time;
double setup_time;

int yyerror(char* msg)
{
  fprintf(stderr, "parser: %s\n", msg);
  return 0;
}

void test() {
  /*
    test_list();
    test_term();
    test_poly();

    test_expr_to_ydt(&type_list, &func_list_list);
    test_ydt_merge(&type_list, &func_list_list);
    test_ydt_to_poly(&type_list, &func_list_list);

    test_groebner();
    test_intersect();

    test_sharp(&type_list, &func_list_list);
    test_I_dp(&type_list, &func_list_list);

    test_list_sort();

    test_poly_divide();
    test_ideal_divide();
  */

  test_I_pd(&type_list, &func_list_list);
}

int main(int argc, char** argv) {
  list_init(&type_list, sizeof(Type), type_free);
  list_init(&func_list_list, sizeof(List), func_body_list_free);

  yyparse();

  int eq_neq = atoi(argv[1]);
  int q_i    = atoi(argv[2]);
  int q_j    = atoi(argv[3]);
  int d      = atoi(argv[4]);

  // ydt to poly
  List ydts;
  list_init(&ydts, sizeof(Ydt), ydt_free);
  expr_to_ydts(&ydts, &type_list, &func_list_list);

  ydt_print((Ydt*) ydts.head->data);
  if(ydts.head->next != NULL) {
    ydt_print((Ydt*) ydts.head->next->data);
  }

  Ydt merge;
  ydt_init(&merge);
  Ydt* ydt1 = (Ydt*) ydts.head->data;
  if(ydts.head->next != NULL) {
    Ydt* ydt2 = (Ydt*) ydts.head->next->data;
    ydt_merge(&merge, ydt1, ydt2);
  } else {
    merge = *ydt1;
  }

  ydt_print(&merge);

  YdtPoly merge_poly;
  ydt_poly_init(&merge_poly);
  ydt_to_ydt_poly(&merge_poly, &merge);
  ydt_poly_print(&merge_poly);
  //

  int dim = dimension(merge_poly.states_size, merge_poly.max_rank);

  // H_qq'
  int z = z_base(merge_poly.states_size, merge_poly.max_rank);
  Poly zq0_zqp0;
  poly_init(&zq0_zqp0, dim);
  Term zq0, zqp0;
  term_init(&zq0, dim);
  term_init(&zqp0, dim);
  term_set_ui(&zq0, 1);
  term_set_ui(&zqp0, 1);
  zq0.mi[z+q_i*2] = 1;
  zqp0.mi[z+q_j*2] = 1;

  poly_sub_t(&zq0_zqp0, &zqp0);
  poly_add_t(&zq0_zqp0, &zq0);

  poly_print(&zq0_zqp0);
  newline();
  //

  ideal_time = 0;
  convert_time = 0;
  setup_time = 0;

  struct rusage usage;
  struct timeval ut1, ut2;
  struct timeval st1, st2;
  struct timeval s, e;
  if(eq_neq == 1) { // 等価であることを調べる
    Ideal ideal;
    ideal_init(&ideal, dim);

    gettimeofday(&s, NULL);
    getrusage(RUSAGE_SELF, &usage);
    ut1 = usage.ru_utime;
    st1 = usage.ru_stime;
    
    I_pd(&ideal, &merge_poly, d);
    
    getrusage(RUSAGE_SELF, &usage);
    ut2 = usage.ru_utime;
    st2 = usage.ru_stime;
    gettimeofday(&e, NULL);

    printf("[==================================================]\n");
    printf("ideal: ");
    ideal_print(&ideal);
    newline();
    printf("I_p%d contains ", d);
    poly_print(&zq0_zqp0);
    printf(": %d\n", ideal_contain(&ideal, &zq0_zqp0));
    printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
    printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);
    printf("ideal: %lf\n", ideal_time);
    printf("convert: %lf\n", convert_time);
    printf("setup: %lf\n", setup_time);
    printf("[==================================================]\n");

    ideal_free(&ideal);
  } else { // 等価でないことを調べる
    Ideal prev;
    ideal_init(&prev, dim);
    ideal_one(&prev);
    Ideal next;

    gettimeofday(&s, NULL);
    getrusage(RUSAGE_SELF, &usage);
    ut1 = usage.ru_utime;
    st1 = usage.ru_stime;
    
    for(int i = 0; i < d; i++) {
      ideal_init(&next, dim);
      I_dp(&next, &merge_poly, &prev);
      ideal_free(&prev);
      prev = next;
    }
    
    getrusage(RUSAGE_SELF, &usage);
    ut2 = usage.ru_utime;
    st2 = usage.ru_stime;
    gettimeofday(&e, NULL);

    printf("[==================================================]\n");
    printf("ideal: ");
    ideal_print(&next);
    newline();
    printf("I_%dp contains ", d);
    poly_print(&zq0_zqp0);
    printf(": %d\n", ideal_contain(&next, &zq0_zqp0));
    printf("user: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6);
    printf("sys : %lf\n", (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("user+sys: %lf\n", (ut2.tv_sec - ut1.tv_sec) + (ut2.tv_usec - ut1.tv_usec)*1.0E-6 + (st2.tv_sec - ut1.tv_sec) + (st2.tv_usec - st1.tv_usec)*1.0E-6);
    printf("real: %lf\n", (e.tv_sec - s.tv_sec) + (e.tv_usec - s.tv_usec)*1.0E-6);
    printf("ideal: %lf\n", ideal_time);
    printf("convert: %lf\n", convert_time);
    printf("setup: %lf\n", setup_time);    
    printf("[==================================================]\n");

    ideal_free(&next);
  }

  if(ydts.head->next != NULL) {
    ydt_free(&merge);
  }
  list_free(&ydts);
  ydt_poly_free(&merge_poly);
  poly_free(&zq0_zqp0);

  list_free(&type_list);
  list_free(&func_list_list);
}

