#include <stdio.h>
#include <stdbool.h>

#include "util.h"

void string_free(void *data) {
  free(*(char**) data);
}

bool string_print_itr(void *data) {
  printf("%s\n", *(char**) data);
  return true;
}

void* list_copy_string(void* data) {
  char** str = (char**) malloc(sizeof(char*));
  *str = strdup(*(char**) data);
  return str;
}

int string_cmp(void* data1, void* data2) {
  char* str1 = *(char**) data1;
  char* str2 = *(char**) data2;

  return strcmp(str1, str2);
}

void list_print_string(List* list) {
  ListNode *p = list->head;

  while(p != NULL) {
    char *str = *(char**)(p->data);
    printf("%s", str);
    if(p->next != NULL) {
      printf(", ");
    }
    p = p->next;
  }
}

//////////////////////////////////////////////////

int digit(int num) {
  int digit = 0;

  while(num != 0) {
    num /= 10;
    digit++;
  }

  return digit;
}

uli factorial(int n, int r) {
  uli fact = 1;
  for(int i = n; i >= r; i--) {
    fact *= i;
  }

  return fact;
}

uli combination(int n, int r) {
  if(r > n / 2) {
    r = n - r;
  }
  uli comb = factorial(n, n-r+1) / factorial(r, 1);
  return comb;
}

uli multi_choose(int n, int r) {
  uli mc = combination(n+r-1, r);
  return mc;
}
