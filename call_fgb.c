#include <stdio.h>
#include <stdlib.h>

#include "call_fgb_basic.h"
#include "call_fgb.h"

#include "protocol_maple.h"

#include "gmp.h"

void FGB(PowerSet)(UI32 bl1,UI32 bl2,char** liste)
{
  FGB(reset_expos)(bl1,bl2,liste);
}
#if LIBMODE EQ 2
int FGb_verb_info=0;
#else
extern int FGb_verb_info;
#endif /*  LIBMODE EQ 2 */
#ifndef CALL_FGB_DO_NOT_DEFINE
FILE* log_output;
void info_Maple(const char* s)
{
  if (FGb_verb_info)
    {
      fprintf(stderr,"%s",s);
      fflush(stderr);
    }
}

void FGb_int_error_Maple(const char* s)
{
  fprintf(stderr,"%s",s);
  fflush(stderr);
  exit(3);
}

void FGb_error_Maple(const char* s)
{
  FGb_int_error_Maple(s);
}

void init_FGb_Integers()
{
  FGB(init_urgent)(4,2,"DRLDRL",100000,0); /* Do not change the following parameters
                                              4 is the number of bytes of each coefficients 
                                              so the maximal prime is <2^32
                                              2 is the number of bytes of each exponent : 
                                              it means that each exponent should be < 2^15 */
  FGB(init)(1,1,0,log_output);/* do not change */
  FGB(reset_coeffs)(1); /* We compute in Q[x1,x2,x3,x4,x5,x6] */
}

void threads_FGb(int t)
{
  I32 code=FGb_int_internal_threads(t);
  code=FGb_internal_threads(t);
}

void FGb_checkInterrupt()
{
}

void FGb_int_checkInterrupt()
{
}

void FGb_push_gmp_alloc_fnct(void *(*alloc_func) (size_t),
			     void *(*realloc_func) (void *, size_t, size_t),
			     void (*free_func) (void *, size_t))
{
}

void FGb_pop_gmp_alloc_fnct()
{
}
#else
extern FILE* log_output;
#endif /* ndef CALL_FGB_DO_NOT_DEFINE */

#if LIBMODE EQ 1
/* init FGb for modular computations */
void init_FGb_Modp(const int p)
{
  FGB(init_urgent)(2,MAPLE_FGB_BIGNNI,"DRLDRL",100000,0); /* meaning of the second parameter:
							     2 is the number of bytes of each coefficients 
							     so the maximal prime is 65521<2^16
							     1 is the number of bytes of each exponent : 
							     it means that each exponent should be < 128 */
  FGB(init)(1,1,0,log_output); /* do not change */
  if (p>65521)
    {
      exit (1);
    }
  {
    UI32 pr[]={(UI32)(p)}; 
    FGB(reset_coeffs)(1,pr);
  }
}
#endif /*  LIBMODE EQ 1 */

UI32 FGB(fgb)(Dpol* p,UI32 np,Dpol* q,UI32 nq,double* t0,FGB_Options opt)
{
  FGb_verb_info =opt->_verb;
  return FGB(groebner)(p,np,q,opt->_mini,opt->_elim,t0,opt->_bk0,opt->_step0,opt->_elim0,&(opt->_env));
}
