#ifndef _YDT_TO_POLY_H_
#define _YDT_TO_POLY_H_

#include "poly.h"
#include "ydt.h"

typedef struct {
  YdtState state;
  YdtInput input;
  int max_rank;
  int states_size;
  Poly poly[2];
} PolyRule;

void poly_rule_init(PolyRule* poly_rule, int states_size, int max_rank);
void poly_rule_free(void* data);

#define dimension(states_size, max_rank)                  \
  (1 + (max_rank * states_size * 2) + (states_size * 2))

// i: index of variable
// s: index of state
// k: 0 or 1
#define x_ijk(max_rank, i, j, k)                \
  (1 + (max_rank * 2 * j) + 2 * i + k)

#define z_base(states_size, max_rank)           \
  (1 + states_size * max_rank * 2)

#define z_jk(base, j, k)                        \
  (base + (2 * j + k))

int z_to_x(int max_rank, int z_base, int index_z, int i);

typedef struct {
  List states;
  List inputs;
  int max_rank;
  int states_size;
  List rules;
} YdtPoly;

void ydt_poly_init(YdtPoly* ydt_poly);
void ydt_poly_free(void* data);

void ydt_to_ydt_poly(YdtPoly* ydt_poly, Ydt* ydt);
void ydt_poly_print(void* data);

#endif
